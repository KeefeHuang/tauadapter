"""
@package tauadapter
This module is part of the tauadapter module, which is used to control execution of TAU
simulations via the tau-python API and preCICE. This module defines the
configuration jsonobject classes used to store data during runtime.
There are three classes defined here, the Interface and Config files.

Interface:
    The Interface class stores data for a single preCICE mesh. Each Interface object contains the
    name of the mesh, a list of boundary markers, size of buffer if boundary markers are not
    defined, list of strings of data types to be read and list of strings of data types to be
    written.

Simulation:
    The Simulation class stores data during the coupled simulation. This is used as a shared
    pointer to share data between various DataHandlers

Config:
    The base Config class is meant to store simulation input data. This sets the folder
    structure, type of Solver subclass et cetera.
"""
import jsonobject


class Simulation(jsonobject.JsonObject):
    """ Simulation class represents a jsonobject that stores the data needed to configure the
    TAU simulation.

    Attributes:
        iteration (int): current coupled iteration number as int. used to set starting iteration
        if restarting
    """
    iteration = jsonobject.IntegerProperty(default=0)


class Interface(jsonobject.JsonObject):
    """ Interface class represents a jsonobject that stores the data for each data interface between
     TAU and preCICE. Contains data that allows calculation of size of data buffer and exact vertex
     ids on interface.

    Attributes:
        name (str): name of the data interface
        mesh (str): name of the data interface mesh, must match mesh name in preCICE
        boundary_markers (list): list of lists of boundary markers, must match markers in TAU
        size (int): if boundary markers are not defined, the size of buffer is set by size value
        read_data (list): list of strings with the data types read from this interface
        write_data (list): list of strings with the data types written to this interface
    """

    name = jsonobject.StringProperty()
    mesh = jsonobject.StringProperty()
    boundary_markers = jsonobject.ListProperty(int)
    size = jsonobject.IntegerProperty()
    read_data = jsonobject.ListProperty(str)
    write_data = jsonobject.ListProperty(str)


class Config(jsonobject.JsonObject):
    """ Config class represents a jsonobject that stores the data needed to configure the tau-python
     precice interface. This stores input parameters that define the environment, folder structure
     used by the coupled simulation.

    Attributes:
        logging_location (str): logging file folder location as str
        logging_name (str): name of logging file as str
        deform_location (str): deformed grid folder location as str
        deform_prefix (str): prefix for deformed grid as str
        scatfile_location (str): scatfile folder location as str
        scatfile_prefix (str): prefix for scatfiles as str

        debug (bool): boolean flag. debug values will be printed in log if set to true. default to
        false if not set
        precice (bool): boolean flag. will run in stand-alone (without preCICE if false). default to
        true if not set
        preprocessing (bool): boolean flag. preprocessing will be run if set to true, default value
        is false
        partitioning (bool): boolean flag. partitioning of nodes to separate MPI processes will be
        run if set to true (restart dicts will be ignored), default value is false
        deformation (bool): boolean flag. deformation required if set to true, default value is
        false

        participant (str): name of preCICE participant. Needs to match participant in precice-config
        parafile (str): location of parafile for TAU as str
        precice_config (str): relative or absolute path to precice configuration xml file as str
        solver (str): name of solver type, used by adapter.Adapter to determine solver, as str.
        Solver subclass to instantiate

        interfaces (list): list of Interface class objects used in the simulation
    """

    logging_location = jsonobject.StringProperty(required=True)
    logging_name = jsonobject.StringProperty(required=True)

    output_location = jsonobject.StringProperty()
    output_prefix = jsonobject.StringProperty()

    deform_location = jsonobject.StringProperty()
    deform_prefix = jsonobject.StringProperty()

    scatfile_location = jsonobject.StringProperty()
    scatfile_prefix = jsonobject.StringProperty()

    debug = jsonobject.BooleanProperty(default=False)
    precice = jsonobject.BooleanProperty(default=True)
    preprocessing = jsonobject.BooleanProperty(default=False)
    partitioning = jsonobject.BooleanProperty(default=False)
    deformation_required = jsonobject.BooleanProperty(default=False)

    participant = jsonobject.StringProperty(required=True)
    partner = jsonobject.StringProperty()
    shared_folder = jsonobject.StringProperty()
    parafile = jsonobject.StringProperty(required=True)
    precice_config = jsonobject.StringProperty()
    solver = jsonobject.StringProperty(required=True)

    interfaces = jsonobject.ListProperty(jsonobject.ObjectProperty(Interface))
    simulation = jsonobject.ObjectProperty(Simulation)
