"""
@package tauadapter
This module is part of the tauadapter package. This contains the utility methods used by
multiple classes in this package.
"""

import sys
import logging
from os.path import join, isdir
from os import makedirs
from copy import deepcopy
import yaml
import netCDF4
import numpy as np
from scipy.sparse import coo_matrix
from mpi4py import MPI
from tauadapter.config import Config


def config(config_file, config_class=Config):
    """ Function that reads configuration file text and converts it to a dictionary

    Arguments:
        config_file (str): name of config file as str
        config_class (class): specific class used to read config info

    Returns:
        Dictionary of values in configuration file
    """
    with open(config_file, "r") as config_data:
        config_dict = yaml.load(config_data, Loader=yaml.SafeLoader)

    base_config = config_class(config_dict)

    return base_config


def initialize_logging(config_dict):
    """ Function that initializes logging. Sets logger as 'tau' by default.

    Arguments:
        config_dict (config.Config): config.Config jsonobject object created from input yaml
        file.
    """
    rank = MPI.COMM_WORLD.Get_rank()
    logger = logging.getLogger("tau" + str(rank))
    logger.setLevel(logging.DEBUG)
    f_handler = logging.FileHandler(
        join(config_dict.logging_location, config_dict.logging_name + str(rank) + ".log"), mode='w')
    if not isdir(config_dict.logging_location):
        if rank == 0:
            create_necessary_folder(config_dict.logging_location, logger)
        MPI.COMM_WORLD.Barrier()
    f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    if config_dict.debug:
        f_handler.setLevel(logging.DEBUG)
    else:
        f_handler.setLevel(logging.INFO)
    f_handler.setFormatter(f_format)
    logger.addHandler(f_handler)

    if rank == 0:
        c_handler = logging.StreamHandler()
        c_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        c_handler.setLevel(logging.INFO)
        c_handler.setFormatter(c_format)
        logger.addHandler(c_handler)

    logger.info("Logger setup")


def create_necessary_folder(folder_name, logger):
    """ Creates a necessary folder. If folder does not exist or cannot be created, the program
    will exist with error signal

    Arguments:
        folder_name (str): key for folder in utility.Config object
        logger (logging.Logger): logger object to log result of folder creation
    """
    logger.info("Trying to make folder: {}".format(folder_name))
    try:
        makedirs(folder_name)
        logger.info("%s folder created", folder_name)
    #     Broad exception is used because exceptions are stored in log file
    except Exception:  # pylint: disable=broad-except
        # broad exception allowed since we are storing exception data
        logger.exception("Unable to create a %s folder", folder_name)
        sys.exit(1)


def read_grid(grid_file):
    """Function that reads TAU grid file. Accepts any netCDF file.

    Arguments:
        grid_file: name of TAU grid file as str

    Returns:
        Returns data in TAU grid file as a standard dictionary
    """
    grid_data = {}
    input_data = netCDF4.Dataset(grid_file)
    for key in input_data.variables:  # pylint: disable=not-an-iterable
        grid_data[key] = input_data[key][:]
    return grid_data


def swap_axis_yz(axis_array):
    """ Function created to swap y and z axis.
    TAU uses XZY format while preCICE accepts XYZ format.
    This function should be called to swap axis.

    Arguments:
        axis_array: 1D array in format [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]

    Returns:
        1D array in format [x0, z0, y0, x1, z1, y1, ..., xn, zn, yn]
    """
    axis_array = np.array(axis_array)
    assert axis_array.shape[0] % 3 == 0, "Data passed to write_grid_deformation is not divisble" \
                                         " by 3, please check passed data"
    assert axis_array.ndim == 1, "Data passed to write_grid_deformation must be a 1D array or list"
    swap_array = deepcopy(axis_array[1::3])
    axis_array[1::3] = axis_array[2::3]
    axis_array[2::3] = swap_array

    return axis_array


def write_grid_deformation(scatfile_name, deformation_data):
    """Function writes the TAU grid deformation scatfile.

    Arguments:
        scatfile_name: name of scatfile as str
        deformation_data: 1d array of deformation data in [x,y,z,dx,dy,dz] format
        x,y,z is the location of the deformation
        dx,dy,dz is the deformation values in x,y,z directions
    """
    data = np.array(deformation_data)
    length = int(data.shape[0])
    assert length % 6 == 0, "Data passed to write_grid_deformation is not divisble by 6, please " \
                            "check passed data"
    assert data.ndim == 1, "Data passed to write_grid_deformation must be a 1D array or list"
    with open(scatfile_name, "w") as grid_data:
        grid_data.write(str(length // 6) + "\r\n")
        for i in range(length // 6):
            grid_data.write(
                "{:3.5f} {:3.5f} {:3.5f} {:3.5f} {:3.5f} {:3.5f}\r\n".format(
                    data[i * 6],
                    data[i * 6 + 1],
                    data[i * 6 + 2],
                    data[i * 6 + 3],
                    data[i * 6 + 4],
                    data[i * 6 + 5]
                ))


def rotation_matrix(angle, axis, dim='rad'):
    """Get direction cosine matrix for a rotation about a cardinal axis.

    Rotates the axes, not the points.

    Arguments:
        angle (float): angle of rotation [radians/degrees] (determined by dim)
        axis (int): axis to rotate about (1, 2, or 3)
        dim (string): units to measure angle in ('rad': radians (default),
            'deg': degrees)

    Returns:
        array (float): array for a rotation of 'angle' deg/rad about axis
            'axis' (size: 3x3)

    Raises:
        ValueError: axis is not 1, 2, or 3

    """

    if dim == 'deg':
        angle = np.radians(angle)  # pylint: disable=assignment-from-no-return

    if axis == 1:
        return np.array([[1, 0, 0], [0, np.cos(angle), -np.sin(angle)],
                         [0, np.sin(angle), np.cos(angle)]])
    elif axis == 2:
        return np.array([[np.cos(angle), 0, np.sin(angle)], [0, 1, 0],
                         [-np.sin(angle), 0, np.cos(angle)]])
    elif axis == 3:
        return np.array([[np.cos(angle), -np.sin(angle), 0], [np.sin(angle),
                                                              np.cos(angle), 0], [0, 0, 1]])

    raise ValueError('axis must be either 1, 2, or 3')


def sparse_rotation_matrix(angle, axis, dim='rad'):
    """Get sparse direction cosine matrix for a rotation about a cardinal axis.

    Rotates the axes, not the points.

    Arguments:
        angle (float): angle of rotation [radians/degrees] (determined by dim)
        axis (int): axis to rotate about (1, 2, or 3)
        dim (string): units to measure angle in ('rad': radians (default),
            'deg': degrees)

    Returns:
        array (float): array for a rotation of 'angle' deg/rad about axis
            'axis' (size: 3x3)

    Raises:
        ValueError: axis is not 1, 2, or 3

    """

    if dim == 'deg':
        angle = np.radians(angle)  # pylint: disable=assignment-from-no-return

    if axis == 1:
        return coo_matrix([[1, 0, 0], [0, np.cos(angle), -np.sin(angle)],
                           [0, np.sin(angle), np.cos(angle)]])
    elif axis == 2:
        return coo_matrix([[np.cos(angle), 0, np.sin(angle)], [0, 1, 0],
                           [-np.sin(angle), 0, np.cos(angle)]])
    elif axis == 3:
        return coo_matrix([[np.cos(angle), -np.sin(angle), 0],
                           [np.sin(angle), np.cos(angle), 0],
                           [0, 0, 1]])

    raise ValueError('axis must be either 1, 2, or 3')


def order_rotation_matrix(*matrices, **kwargs):
    """ Method to order rotational matrices based on given ordering, default is 213

    Arguments:
        order (str): list of integers to order provide matrices
        matrices (list): list of matrices to order

    Returns:
        Matrix ordered per provided order
    """
    order = str(kwargs['order'])
    matrices = np.array(matrices)
    assert len(order) == matrices.shape[0], "Number of matrices to order must be same as the " \
                                            "order list"

    temp_order = []
    for char in order:
        temp_order.append(int(char) - 1)
    matrices = matrices[temp_order]

    result = matrices[0]
    for matrix in matrices[1:]:
        result = result.dot(matrix)

    return result


def shift_coefficients(rotation_coefficients, shift):
    """ Method to shift coefficients of fourier coefficients by a set angle,
    such that the shifted coefficients (s_c) are as follows

    c0 + c1*cos(kwt) + c2*sin(kwt) + ... =
    s_c0 + s_c1*cos(kwt + shift) + s_c2*sin(kwt + shift) + ...

    See notes for explanation of calculation

    Arguments:
        rotation_coefficients (ndarray): numpy array of fourier coefficients, ordered as
            [c0, c1, c2, c3, ...] -> c0 + c1*cos(kwt) + c2*sin(kwt) + c3*cos(2*kwt) + c4*sin(2*kwt)
            ...

        shift (float): angle in radians to shift fourier coefficients

    Note:
        When shifting fourier coefficients by angle shift:
        sin(a + b) = sin(a)cos(b) + cos(a)sin(b)
        cos(a + b) = cos(a)cos(b) - sin(a)sin(b)
        Ex.
        c0 + c1*cos(kwt + shift) + c2*sin(kwt + shift) =
        c0 + [c1*cos(shift) + c2*sin(shift)]*cos(kwt) + [-c1*sin(shift) + c2*cos(shift)]
        s_c1 = c1*cos(shift) + c2*sin(shift)
        s_c2 = -c1*sin(shift) + c2*cos(shift)
    """
    shifted_rotation_coefficients = np.zeros(rotation_coefficients.shape)
    shifted_rotation_coefficients[0] = rotation_coefficients[0]
    original_cosine_coefficients = rotation_coefficients[1::2]
    original_sine_coefficients = rotation_coefficients[2::2]

    shifted_rotation_coefficients[1::2] = np.round(
        np.multiply(original_cosine_coefficients, np.cos(shift)) +
        np.multiply(original_sine_coefficients, np.sin(shift)), 17)

    shifted_rotation_coefficients[2::2] = np.round(
        np.multiply(original_cosine_coefficients, -np.sin(shift)) +
        np.multiply(original_sine_coefficients, np.cos(shift)), 17)

    return shifted_rotation_coefficients
