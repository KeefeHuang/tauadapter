"""
@package tauadapter
This module is a part of the tauadapter module. This module contains the
Adapter class that allows the user to control the TAU solver at a high level of abstraction.

Notes:
    - mpi4py library is required to run TAU in parallel. this cannot be removed if preCICE Is run
        without MPI
    - additional pylint settings to are to avoid pylint errors when writing code in environment
        where TAU or preCICE are not recognized when using python 2.7 interpreters
"""

import sys
from os.path import join
import logging
from mpi4py import MPI
import precice  # pylint: disable=import-error

import tau_python  # pylint: disable=import-error

import tauadapter.utility as utility
from tauadapter.datainterface import DataInterface
from tauadapter.solver import Solver

from tauadapter.tight.tightsolver import TightSolver
from tauadapter.helicopter.helicoptersolver import HelicopterSolver


class Adapter(object):
    """ The Adapter class controls the DataInterface and Solver classes. Oversees the
    instantiation of the two classes and their interactions. This class acts as the user
    interface for the tauadapter module.

    Attributes:
        config_file (str): name of the adapter config file as str
        config (config.Config): utility.Config dictionary object created from input config file,
        parsed with utility.config()
        comm (MPI.COMM_WORLD): communication handle for MPI
        logger (logging.Logger): logger object
        solver (solver.Solver): Solver class object that controls the execution of the main
            simulation loop
        data_interface (datainterface.DataInterface): interface.DataInterface object that
        controls the
            passing of information from TAU to preCICE
        precice_interface (precice.Interface): preCICE interface object
    """

    SOLVER_TYPES = {"TightSolver": TightSolver, "HelicopterSolver": HelicopterSolver}
    """
    Dictionary of all possible solver types. Used for runtime choice of solver class
    """

    def __init__(self, config_file):
        """ Constructor for Adapter class.
        Reads config dict that controls preCICE interface and TAU environment instantiation.\n
        Initializes the logging for the simulation.\n

        Arguments:
            config_file (str): name of tauadapter yaml config file
        """
        self.config_file = config_file
        self.config = utility.config(config_file)
        self.comm = MPI.COMM_WORLD

        # Initialize logging with config dict
        utility.initialize_logging(self.config)

        self.logger = logging.getLogger("tau" + str(self.comm.Get_rank()) + ".adapter")

        self.data_interface = None
        self.precice_interface = None
        self.solver = None
        self.solver_type = None

    def initialize(self):
        """ Method to initialize solving environment. Initializes the required solver.Solver object.
         Initializes the TAU-Python environment in serial or parallel depending on config file.
         Initializes and configures the preCICE environment (if preCICE is used) and sets up the
         data interface.
        """
        # Run the preCICE initialization step if required
        if self.config.precice:
            self.initialize_precice()
            self.data_interface = DataInterface(self.precice_interface, self.config)
            # Setup mesh interfaces and read/write handlers

        self.initialize_tau_python()
        self.initialize_solver()

    def initialize_solver(self):
        """ Method to initialize solver using SOLVER_TYPES dict
        """
        if self.solver_type is not None:
            self.logger.info("Instantiating solver %s", self.solver_type)
            self.solver = self.solver_type(self.config_file, self.data_interface)
        else:
            # Searches for a valid solver type from the 'solver_types' dict.
            try:
                _ = self.SOLVER_TYPES[str(self.config.solver)]
            except KeyError:
                self.logger.error("Solver type %s in file doesn't exist in config file",
                                  (str(self.config.solver)))
                sys.exit(1)
            self.logger.info("Instantiating solver %s", self.SOLVER_TYPES[str(self.config.solver)])
            self.solver = self.SOLVER_TYPES[str(self.config.solver)](self.config_file,
                                                                     self.data_interface)

        # Runs preprocessing
        if self.config.preprocessing:
            self.solver.preprocessing()

    def initialize_tau_python(self):
        """ Method to initialize TAU-Python environment. Creates TAU-Python logging file
        in log folder set in config.Config object.
        """
        # Get logging location
        log_file = str(join(self.config.logging_location, self.config.logging_name))
        # Arguments to initialize the TAU-Python environment
        tau_init_args = ['', '', self.config_file, log_file]
        # Checks if multiple processes are running, and initializes TAU-Python in parallel
        if self.comm.Get_size() > 1:
            tau_init_args = ['', '', self.config_file, log_file, 'use_mpi']
            self.logger.info("Initializing the TAU-Python environment in parallel.")
        else:
            self.logger.info("Initializing the TAU-Python environment in serial.")
        tau_python.tau_init(len(tau_init_args), tau_init_args)

    def initialize_precice(self):
        """ Method to initialize preCICE environment.
        """
        self.logger.info("Starting preCICE...")
        # Instantiates the preCICE interface if necessary
        try:
            precice_config = str(self.config.precice_config)
        except IOError:
            self.logger.exception("Error: preCICE config file '%s' not found.",
                                  self.config.precice_config)
            sys.exit(1)
        try:
            self.precice_interface = precice.Interface(str(self.config.participant),
                                                       self.comm.Get_rank(),
                                                       self.comm.Get_size())
            self.precice_interface.configure(precice_config)
        except TypeError:
            self.precice_interface = precice.Interface(str(self.config.participant),
                                                       precice_config,
                                                       self.comm.Get_rank(),
                                                       self.comm.Get_size())

        self.logger.info('preCICE configured...')

    def update_solver(self, solver_class):
        """  Method to add a single new solver type

                Arguments:
                    solver_class (class): class of new Solver
        """
        if not issubclass(solver_class, Solver):
            self.logger.error("%s is not a subclass of Solver, please only add "
                              "Solver subclasses", solver_class)
            return
        self.solver_type = solver_class

    def update_config(self, config_class):
        """  Method to add a new Config type

        Arguments:
            config_class (class): class of new Config
        """
        if self.solver is not None:
            self.logger.error("Please call update_config() before calling initialize()")
            sys.exit(1)
        else:
            if self.solver_type is None:
                Solver.CONFIG_CLASS = config_class
            else:
                self.solver_type.CONFIG_CLASS = config_class

    def add_new_data_handler_types(self, class_dict, data_handler_type):
        """ Method to add new datahandlers to coupled simulation

        Arguments:
            class_dict (dict): dict containing key-value pair, where key is the name of the new
                datahandler and value is the datahandler class type.
            data_handler_type (str): updates DATA_READERS or DATA_WRITERS dict depending on input
        """
        if self.solver is None:
            self.logger.info("Attempting to add data handler before solver is initialized. Please "
                             "initialize solver first")
        self.solver.add_new_data_handler_type(class_dict, data_handler_type)

    def add_new_data_handler_type(self, data_handler_name, data_handler_class, data_handler_type):
        """  Method to add a single new datahandler to coupled simulation

        Arguments:
            data_handler_name (str): name of datahandler in config file
            data_handler_class (class): class of new data handler
            data_handler_type (str): updates DATA_READERS or DATA_WRITERS dict depending on input
        """
        if self.solver is None:
            self.logger.info("Attempting to add data handler before solver is initialized. Please "
                             "initialize solver first")
        class_dict = {data_handler_name: data_handler_class}
        self.solver.add_new_data_handler_type(class_dict, data_handler_type)

    def setup_data_handlers(self):
        """ Sets up datahandlers in DataInterface"""
        self.solver.setup_data_handlers()

    # Set the deformation function if preCICE is not necessary
    def set_deformation_function(self, function):
        """ Sets the deformation function used by TAU if preCICE is not used and
        deformation is required
        Function should output strings organised in [x, y, z, dx, dy, dz] format
        such that x,y,z is the location where deformation occurs and dx, dy, dz
        are the deformations in the x-, y-, and z-axis respectively.

        Arguments:
            function (function): function handle to deformation function
        """
        self.solver.deformation_function = function

    # Execute the Solver class
    def execute(self):
        """ Calls the execution function in the instantiated solver (solvers.Solver
        subclass). Calls solver.Solver.execute_precice()
        function if preCICE is used. Otherwise calls implemented solver.Solver.execute() function.
        """
        if self.config.precice:
            self.solver.execute_precice()
        else:
            self.solver.execute()

    def __del__(self):
        """ Destructor for the Adapter class.
        Calls the destructor for the TAU-Python environment
        """
        tau_python.tau_close()
