"""
@tauadapter
This module contains constants used throughout the package. These contants are used as key values
for the dictionary passed when instantiating DataHandler subclasses.
"""
DATA_ID = 'data_id'
MESH_ID = 'mesh_id'
CONFIG = 'config'
INTERFACE = 'interface'
COORDINATES = 'coordinates'
VERTEX_IDS = 'vertex_ids'
PRECICE_VERTICES = 'precice_vertex_ids'
PARAFILE = 'parafile'
NAME = 'name'
DATA_TYPE = 'data_type'
COMM = 'comm'
