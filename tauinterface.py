"""
@package tauadapter
This module is part of the tauadapter module. This module contains the interface with TAU. This
module defines the TauInterface class, which sets up and reads data from TAU memory buffers. This is
used to extract data from TAU during runtime
"""
import logging
import numpy as np

from mpi4py import MPI
import tau_python  # pylint: disable=import-error


class TauInterface(object):
    """ This class reads data from the various streams that are buffered during simulation and
    enables the extraction of TAU data at run-time.

    Attributes:
        f_type (class): class of float used by TAU in this environment
        i_type (class): class of integer used by TAU in this environment
        logger (logging.Logger): logging object used to log output
    """

    def __init__(self):
        """ Constructor probes the size of TAU double and integers and selects the matching numpy
        datatype"""
        tau_double_size = tau_python.get_size_of_TauDouble()
        if tau_double_size * 8 == 16:
            self.f_type = np.float16
        elif tau_double_size * 8 == 32:
            self.f_type = np.float32
        elif tau_double_size * 8 == 64:
            self.f_type = np.float64
        elif tau_double_size * 8 == 128:
            self.f_type = np.float128

        tau_int_size = tau_python.get_size_of_TauIndex()
        if tau_int_size * 8 == 16:
            self.i_type = np.int16
        elif tau_int_size * 8 == 32:
            self.i_type = np.int32
        elif tau_int_size * 8 == 64:
            self.i_type = np.int64
        elif tau_int_size * 8 == 128:
            self.i_type = np.int128

        rank = MPI.COMM_WORLD.Get_rank()
        self.logger = logging.getLogger("tau" + str(rank) + ".TauInterface")

    def get_int_variable(self, stream_name, data_name):
        """ Gets int variable data_name from given stream_name.

        Arguments:
            stream_name (str): Name of stream to probe
            data_name (str): Name of data to be returned.

        Returns:
            integer ndarray of type self.i_type containing data_name variable from stream
            stream_name
        """
        data_buffer = tau_python.tau_numpy_get_variable(stream_name, data_name)

        if data_buffer:
            return np.frombuffer(data_buffer, dtype=self.i_type)

        self.logger.warning("Either buffer %s does not exist or data %s does not exist in "
                            "buffer", stream_name, data_name)
        return None

    def get_float_variable(self, stream_name, data_name):
        """ Gets float variable data_name from given stream_name.

        Arguments:
            stream_name (str): Name of stream to probe
            data_name (str): Name of data to be returned.

        Returns:
            integer ndarray of type self.f_type containing data_name variable from stream
            stream_name
        """
        data_buffer = tau_python.tau_numpy_get_variable(stream_name, data_name)

        if data_buffer:
            return np.frombuffer(data_buffer, dtype=self.f_type)

        self.logger.warning("Either buffer %s does not exist or data %s does not exist in "
                            "buffer", stream_name, data_name)
        return None

    def get_connectivity_data(self, connectivity_type):
        """ Gets connectivity data of connectivity type from TAU if it exists. TAU provides the
        following connectivity data:
            points_of_hexaeders,
            points_of_tetraeders,
            points_of_pyramids,
            points_of_prisms,
            points_of_surfacequadrilaterals,
            points_of_surfacetriangles
        """
        data = tau_python.tau_numpy_connectivity(connectivity_type)

        if data:
            return data

        self.logger.warning("Connectivity type %s is not available in this simulation",
                            connectivity_type)
        return None
