"""
@package tauadapter
This module is part of the tauadapter module. This module defines the DataInterface class,
which stores and handler a list of DataHandler objects to facilitate communication between
TAU and preCICE.
"""

import logging
import numpy as np
from mpi4py import MPI


class DataInterface(object):
    """ DataInterface class controls the communication between TAU and preCICE\n

    Attributes:
        config (config.Config): Utility.Config dictionary object created from input config file,
            parsed with Utility.config()
        precice_interface (precice.Interface): preCICE interface object. Allows read/write from/to
            preCICE
        data_readers (list): list of datahandler.DataHandler objects that handle data read from
        preCICE interface
        data_writers (list): list of datahandler.DataHandler objects that handle data sent to
            preCICE interface
        logger (logging.Logger): logger object that logs output
        num_data_readers (int): number of data readers as int
        num_data_writers (int): number of data writers as int

    """

    def __init__(self, precice_interface, config):
        """ DataInterface constructor\n
        Sets precice_interface and config member variables

        Arguments:
            precice_interface (precice.Interface): preCICE interface object. Allows read/write to
            preCICE
            config (utility.Config): Utility.Config dictionary object created from input config
            file, parsed with Utility.config()
        """
        rank = MPI.COMM_WORLD.Get_rank()
        self.logger = logging.getLogger("tau" + str(rank) + ".data_interface")

        self.config = config
        self.precice_interface = precice_interface

        self.data_readers = []
        self.data_writers = []

        self.initialized = False

    def add_read_data_hander(self, data_reader):
        """ Adds DataReader object to self.data_readers

        Arguments:
            data_reader (datahandler.DataHandler): DataReader object

        Returns:
            index of added DataReader object in self.data_readers
        """
        self.data_readers.append(data_reader)

        return len(self.data_readers) - 1

    def add_write_data_hander(self, data_writer):
        """ Adds DataWriter object to self.data_readers

        Arguments:
            data_writer (datahandler.DataHandler): DataWriter object

        Returns:
            index of added DataReader object in self.data_readers
        """
        self.data_writers.append(data_writer)
        return len(self.data_writers) - 1

    def read_all(self, iteration):
        """ Function loops through all existing data readers, communicates with preCICE interface
        and passes data to the DataReader objects for post-processing

        Arguments:
            iteration (int): the current coupling iteration
        """
        self.logger.info("Reading data from preCICE at iteration %s", iteration)
        if self.precice_interface.is_read_data_available():
            dimensions = self.precice_interface.get_dimensions()
            for data_reader in self.data_readers:
                if data_reader.is_scalar():
                    data = np.zeros(data_reader.length)
                    self.precice_interface.read_block_scalar_data(
                        data_reader.data_id,
                        data_reader.length,
                        data_reader.precice_vertex_ids,
                        data
                    )
                else:
                    data = np.zeros((dimensions * data_reader.length))
                    self.precice_interface.read_block_vector_data(
                        data_reader.data_id,
                        data_reader.length,
                        data_reader.precice_vertex_ids,
                        data
                    )
                data_reader.read(data, iteration)
        self.logger.info("Completed reading data to preCICE at iteration %s", iteration)

    def write_all(self, iteration):
        """ Function loops through all existing data writers, communicates with preCICE interface
        and passes data from the DataWriter objects to preCICE

        Arguments:
            iteration (int): the current coupling iteration
        """
        self.logger.info("Writing data to preCICE at iteration %s", iteration)
        for data_writer in self.data_writers:
            data = data_writer.write(iteration)
            if data_writer.is_scalar():
                self.precice_interface.write_block_scalar_data(
                    data_writer.data_id,
                    data_writer.length,
                    data_writer.precice_vertex_ids,
                    data
                )
            else:
                self.precice_interface.write_block_vector_data(
                    data_writer.data_id,
                    data_writer.length,
                    data_writer.precice_vertex_ids,
                    data
                )
        self.logger.info("Completed writing data to preCICE at iteration %s", iteration)

    def read(self, iteration, index):
        """ Function loops through all existing data readers, communicates with preCICE interface
        and passes data to the DataReader objects for post-processing

        Arguments:
            iteration (int): the current coupling iteration
            index (int): index of data reader as int
        """
        self.logger.info("Reading data from preCICE data_reader %s at iteration %s",
                         self.data_readers[index].mesh_id, iteration)
        if self.precice_interface.is_read_data_available():
            dimensions = self.precice_interface.get_dimensions()
            data_reader = self.data_readers[index]
            if data_reader.is_scalar():
                data = np.zeros(data_reader.length)
                self.precice_interface.read_block_scalar_data(
                    data_reader.data_id,
                    data_reader.length,
                    data_reader.precice_vertex_ids,
                    data
                )
            else:
                data = np.zeros((dimensions * data_reader.length))
                self.precice_interface.read_block_vector_data(
                    data_reader.data_id,
                    data_reader.length,
                    data_reader.precice_vertex_ids,
                    data
                )
            data_reader.read(data, iteration)
        self.logger.info("Completed reading data from preCICE data_reader %s at iteration %s",
                         self.data_readers[index].mesh_id, iteration)
        return data

    def write(self, iteration, index):
        """ Function loops through all existing data writers, communicates with preCICE interface
        and passes data from the DataWriter objects to preCICE

        Arguments:
            iteration (int): the current coupling iteration
            index (int): index of data reader as int
        """
        self.logger.info("Writing data from preCICE data_writer %s at iteration %s",
                         self.data_readers[index].mesh_id, iteration)
        data_writer = self.data_writers[index]
        data = data_writer.write(iteration)
        if data_writer.is_scalar():
            self.precice_interface.write_block_scalar_data(
                data_writer.data_id,
                data_writer.length,
                data_writer.precice_vertex_ids,
                data
            )
        else:
            self.precice_interface.write_block_vector_data(
                data_writer.data_id,
                data_writer.length,
                data_writer.precice_vertex_ids,
                data
            )
        self.logger.info("Completed writing data to preCICE at iteration %s", iteration)

    def initialize_data(self, iteration):
        """ Method reads initial data from preCICE for serial coupling case
        """
        if self.initialized:
            return
        self.logger.info("Reading initial data from preCICE")
        if self.precice_interface.is_read_data_available():
            dimensions = self.precice_interface.get_dimensions()
            for data_reader in self.data_readers:
                if data_reader.is_scalar():
                    data = np.zeros(data_reader.length)
                    self.precice_interface.read_block_scalar_data(
                        data_reader.data_id,
                        data_reader.length,
                        data_reader.precice_vertex_ids,
                        data
                    )
                else:
                    data = np.zeros((dimensions * data_reader.length))
                    self.precice_interface.read_block_vector_data(
                        data_reader.data_id,
                        data_reader.length,
                        data_reader.precice_vertex_ids,
                        data
                    )
                data_reader.read(data, iteration)
        self.initialized = True
        self.logger.info("Completed reading data to preCICE at iteration %s", iteration)
