"""
@package tauadapter
This module is part of the tauadapter module. This module defines the DataHandler class,
which is an abstract base class which defines an interface for all future DataHandler
subclasses.
"""

from abc import ABCMeta, abstractmethod
import tauadapter.constants as constant


class DataHandler:
    """ Abstract DataHandler base class that reads data from TAU and writes to preCICE or reads
    data from preCICE and passes data to TAU.

    Attributes:
        config (config.Config): configuration jsonobject used to store simulation input parameters
        coordinates (ndarray): 1D numpy array of coordinates corresponding to precice
            vertex ids
        data_id (int): unique integer id identifying the specific data type in preCICE interface
        interface (config.Interface): interface object that contains data related preCICE interface
        length (int): number of nodes on interface (n) for current process
        mesh_id (int): unique integer id identifying the specific mesh in precice interface
        precice_vertex_ids (ndarray): 1D numpy array of precice vertex ids
        tau_para (tau_python.PyPara): PyPara object used to update parafile
        simulation (config.Simulation): simulation jsonobject used to store and pass simulation
            data between DataHandlers
        vertex_ids (ndarray): 1D ndarray of TAU vertex ids corresponding to precice vertex ids
    """
    __metaclass__ = ABCMeta

    def __init__(self, args):
        self.config = args[constant.CONFIG]
        self.coordinates = args[constant.COORDINATES]
        self.data_id = args[constant.DATA_ID]
        self.interface = args[constant.INTERFACE]
        self.mesh_id = args[constant.MESH_ID]
        self.name = args[constant.NAME]
        self.precice_vertex_ids = args[constant.PRECICE_VERTICES]
        self.tau_para = args[constant.PARAFILE]
        self.vertex_ids = args[constant.VERTEX_IDS]
        self.comm = args[constant.COMM]

        self.simulation = self.config.simulation
        self.length = len(list(self.precice_vertex_ids))

    @abstractmethod
    def write(self, iteration):
        """ Abstract function used to write data to preCICE

        Arguments:
            iteration (int): current coupled iteration as int
        """
        pass

    @abstractmethod
    def read(self, data, iteration):
        """ Abstract function used to read data from preCICE

        Arguments:
            data: data received from preCICE. This is in a 1D array of size (n x 3)
            iteration (int): current coupled iteration as int
        """
        pass

    @abstractmethod
    def is_scalar(self):
        """Function returns True if data type is scalar
        """
        pass
