"""
@package tauadapter
This module contains the DataHandlerCreator class used to create the datahandlers in the helicopter
simulation
"""
import logging
import re
import sys
from mpi4py import MPI

import numpy as np

import tau_python  # pylint: disable=import-error

from tauadapter.tauinterface import TauInterface
from tauadapter.utility import read_grid, rotation_matrix

from tauadapter.datahandler import DataHandler
from tauadapter.tight.vectordatahandler import VectorHandler
from tauadapter.tight.scalardatahandler import ScalarHandler

import tauadapter.constants as constant

# Dictionary of types of data readers used by helicopter simulations
DATA_READERS = {}

# Dictionary of types of data readers used by helicopter simulations
DATA_WRITERS = {}

# List of data types which are supported by vectordatahandler and scalardatahandler
VECTOR_DATA_TYPES = {'Forces': ['x-force', 'y-force', 'z-force']}
SCALAR_DATA_TYPES = {'Pressure': ['p']}


class DataHandlerFactory(object):
    """ Creates DataHandler objects for helicopter simulation

    Attributes:
        config (helicopter.helicopterconfig.HelicopterConfig): HelicopterConfig jsonobject that
        stores input parameters for entire coupled simulation.
        simulation (helicopter.helicopterconfig.Simulation): Simulation jsonobject that stores
        input parameters for TAU simulation
        precice_interface (precice.Interface): preCICE interface class. used to control coupled
            simulation
        data_interface (inteface.DataInterface): DataInterface object. used to pass packaged
            information between TAU adapter and preCICE.
        tau_para (tau_python.PyPara): PyPara object. reads and writes to input TAU parafile
        tau_interface (tauinterface.TauInterface): TauInterface class. sets up and allows access
            to TAU memory buffers during simulation runtime
        rank (int): mpi rank of current process
        logger (logging.Logger): Logger object. logs information from DataHandlerFactory
        partitioned_coords (ndarray): 1D numpy array that contains the coordinates of nodes
            relevant to this process
        partitioned_nids (ndarray): 1D numpy array that contains the global ids of nodes
            relevant to this process
        surf_markers (ndarray): 2D numpy array of surface markers corresponding to each
            quadrilateral element in this simulation
        surf_quad_points (ndarray): 1D numpy array of global node ids of each quadrilateral
            element in this simulation
    """

    def __init__(self, config, precice_interface, data_interface, tau_para):
        self.config = config
        self.simulation = self.config.simulation
        self.precice_interface = precice_interface
        self.data_interface = data_interface
        self.tau_para = tau_para
        self.tau_interface = None
        self.rank = MPI.COMM_WORLD.Get_rank()
        self.comm = MPI.COMM_WORLD

        self.logger = logging.getLogger("tau" + str(self.rank) + ".datahandlerfactory")
        self.partitioned_coords = None
        self.partitioned_nids = None
        self.surf_markers = None
        self.surf_quad_points = None
        self.surf_tri_points = None
        self.newcomm = None

    def initialize(self):
        """ Initialize DataHandlerFactory. Setup access to TAU memory buffers. Cannot be done
        before initialization of TAU PySolv class"""
        self.tau_interface = TauInterface()
        self.partitioned_nids, self.partitioned_coords = self.partition_grid()
        self.surf_markers, self.surf_tri_points, self.surf_quad_points = \
            self.extract_surface_points()

    def add_new_data_handler_type(self, class_dict, handler_type):
        """ Update dictionary of datahandler types

        Arguments:
            class_dict (dict): dict containing key-value pair, where key is the name of the new
            datahandler and value is the datahandler class type.
            handler_type (str): updates DATA_READERS or DATA_WRITERS dict depending on input
        """
        for handler_class in class_dict.values():
            if not issubclass(handler_class, DataHandler):
                self.logger.error("%s is not a subclass of DataHandler, please only add "
                                  "DataHandler subclasses", handler_class)
                return
        if handler_type.lower() == "read":
            self.logger.info("Adding %s classes to %s", class_dict, "data readers")
            DATA_READERS.update(class_dict)
        elif handler_type.lower() == "write":
            self.logger.info("Adding %s classes to %s", class_dict, "data writers")
            DATA_WRITERS.update(class_dict)

    def create_data_handlers(self, interface, restart_dict):
        """ Creates a datahandler object based on input parameters

        Arguments:
            interface (helicopterconfig.Interface): Interface jsonobject that contains
            DataHandler information.
            restart_dict (dict): dict that contains previous partitioning
        """
        mesh_id = self.precice_interface.get_mesh_id(str(interface.mesh))

        if interface.boundary_markers:
            tau_vertex_ids, num_precice_points, precice_coordinates = \
                self.setup_mesh_based_datahandler(interface, restart_dict)
        else:
            num_precice_points, precice_coordinates = self.setup_non_mesh_datahandler(interface)
            tau_vertex_ids = None

        if self.rank == 0:
            if num_precice_points is None:
                ranks = []
            else:
                ranks = [0]
            size = self.comm.Get_size()
            for i in range(1, size):
                ident = self.comm.recv(source=i)
                if ident:
                    ranks.append(ident)
            ranks = self.comm.bcast(ranks, root=0)
            newgroup = self.comm.group.Incl(ranks)
            newcomm = self.comm.Create_group(newgroup)
            if num_precice_points is None:
                return
        else:
            if num_precice_points is None:
                self.logger.debug("Not setting up %s as size is 0", interface.mesh)
                self.comm.send(0, dest=0)
                ranks = None
                ranks = self.comm.bcast(ranks, root=0)
                self.comm.group.Incl(ranks)
                return
            else:
                self.comm.send(self.rank, dest=0)
                ranks = None
                ranks = self.comm.bcast(ranks, root=0)
                newgroup = self.comm.group.Incl(ranks)
                newcomm = self.comm.Create_group(newgroup)
        self.newcomm = newcomm
        precice_vertex_ids = np.zeros(num_precice_points)

        self.logger.debug("Setting up interface mesh %s", str(interface.mesh))
        self.logger.debug("%s", str(precice_coordinates))
        self.precice_interface.set_mesh_vertices(mesh_id, num_precice_points,
                                                 precice_coordinates, precice_vertex_ids)

        data_handler_arguments = {constant.CONFIG: self.config,
                                  constant.COORDINATES: precice_coordinates,
                                  constant.INTERFACE: interface,
                                  constant.MESH_ID: mesh_id,
                                  constant.PARAFILE: self.tau_para,
                                  constant.PRECICE_VERTICES: precice_vertex_ids,
                                  constant.VERTEX_IDS: tau_vertex_ids,
                                  constant.COMM: newcomm}

        for data_name in interface.read_data:
            data_id = self.precice_interface.get_data_id(str(data_name), mesh_id)
            data_handler_arguments[constant.NAME] = data_name
            data_handler_arguments[constant.DATA_ID] = data_id
            _, data_reader = self.create_data_handler(DATA_READERS, data_name,
                                                      data_handler_arguments)
            self.data_interface.add_read_data_hander(data_reader)

        for data_name in interface.write_data:
            data_id = self.precice_interface.get_data_id(str(data_name), mesh_id)
            data_handler_arguments[constant.NAME] = data_name
            data_handler_arguments[constant.DATA_ID] = data_id
            _, data_writer = self.create_data_handler(DATA_WRITERS, data_name,
                                                      data_handler_arguments)
            self.data_interface.add_write_data_hander(data_writer)

        return

    def create_data_handler(self, data_handler_dict, data_name, data_handler_arguments):
        """ Creates a datahandler object based on input parameters

        Arguments:
            data_name (str): data names in given datahandler as str
            data_handler_arguments (dict): list of datahandler arguments
            data_handler_dict(dict): dict that points to type of datahandler to be created
        """
        self.logger.debug("Setting up data handler %s", str(data_name))
        data_type_pattern = re.compile(r"\w[^_0-9]+")
        data_type = re.search(data_type_pattern, str(data_name)).group(0)
        try:
            data_handler = data_handler_dict[data_type](data_handler_arguments)
        except KeyError:
            self.logger.info("Key %s not found in %s, attempting to use generic DataHandlers",
                             data_type, data_handler_dict)
            try:
                data_keys = VECTOR_DATA_TYPES[data_type]
                data_handler_arguments[constant.DATA_TYPE] = data_keys
                self.logger.info("Key found, mapped to TAU data types %s", data_keys)
                data_handler = VectorHandler(data_handler_arguments)
            except KeyError:
                try:
                    data_keys = SCALAR_DATA_TYPES[data_type]
                    data_handler_arguments[constant.DATA_TYPE] = data_keys
                    self.logger.info("Key found, mapped to TAU data types %s", data_keys)
                    data_handler = ScalarHandler(data_handler_arguments)
                except KeyError:
                    self.logger.exception("Key %s not recognised, exitting", data_type)
                    sys.exit(1)

        return data_type, data_handler

    def setup_non_mesh_datahandler(self, interface):
        """ Creates inputs for non mesh-based datahandler. These contain variables that are not
        related to nodes in the TAU mesh. Examples include the azimuth positions or collocation
        points from solid solver.

        Arguments:
            interface (config.Interface): Interface jsonobject that stores data required to
            create datahandler.
        """
        assert interface.size, \
            "If surface markers are not defined, size of interface must be defined"
        self.logger.debug("Setting up Mesh %s, size %s", str(interface.mesh), str(interface.size))
        # Determine number of vertices in mesh
        num_precice_points = interface.size

        # Setting up precice coordinates
        precice_coordinates = np.array(
            [[] + [0, 0, i] for i in range(num_precice_points)]).flatten()

        return num_precice_points, precice_coordinates

    def setup_mesh_based_datahandler(self, interface, restart_dict):
        """ Creates inputs for mesh-based datahandler. Mesh-based data is data read from the
        nodes in TAU outputs.

        Arguments:
            interface (utility.Interface): Interface jsonobject used to set up DataHandler objects
            restart_dict (dict): dictionary of partitioned coordinates and global ids. provided
                so that partitioning does not take place twice
        """
        self.logger.debug("Setting up Mesh %s with boundary_markers %s", str(interface.mesh),
                          str(interface.boundary_markers))

        if str(interface.mesh) in restart_dict:
            tau_vertex_ids, precice_coordinates = restart_dict[str(interface.mesh)]
            try:
                num_precice_points = precice_coordinates.shape[0] // 3
            except AttributeError:
                num_precice_points = None
        else:
            self.logger.debug("Add key to dict %s", str(interface.mesh))
            num_precice_points, precice_coordinates, tau_vertex_ids \
                = self.filter_surface_nodes(interface)

            if precice_coordinates is not None:
                if self.config.solver == "HelicopterSolver":
                    # Blade num indicates the blade position from the origin in a clock-wise fashion
                    blade_angle_spacing = 360 / self.simulation.blades
                    blade_num = interface.blade
                    # Rotate surface coordinates by blade angle
                    precice_coordinates = precice_coordinates.dot(
                        rotation_matrix(blade_angle_spacing * blade_num, 3, dim='deg'))
                precice_coordinates = precice_coordinates.flatten()

            if restart_dict is not None:
                restart_dict[str(interface.mesh)] = (tau_vertex_ids, precice_coordinates)
        # Setting empty np array to store precice vertex ids

        return tau_vertex_ids, num_precice_points, precice_coordinates,

    def extract_surface_points(self):
        """ Method to extract the nodes belonging to surface quadrilaterals with
        boundary markers in configuration file """
        self.logger.info("Extracting surface points")
        # Get name of primary grid file
        primary_grid_filename = self.tau_para.get_para_value("Primary grid filename")
        # Attempt to open primary grid file
        try:
            grid_data = read_grid(primary_grid_filename)
        # Broad exception acceptable here as error text as netCDF4 errors are complex and
        # errors will be logged
        except Exception as error:  # pylint: disable=broad-except
            self.logger.error(error, exc_info=True)
            sys.exit()
        # Returns 1d list of boundary markers corresponding to each point in primary grid
        surf_markers = np.array(grid_data['boundarymarker_of_surfaces'])
        # Returns list of lists, each list contains the nodes pertaining to single surface
        try:
            surf_tri_points = np.array(grid_data['points_of_surfacetriangles'])
        except KeyError:
            surf_tri_points = None

        try:
            surf_quad_points = np.array(grid_data['points_of_surfacequadrilaterals'])
        except KeyError:
            surf_quad_points = None
        return surf_markers, surf_tri_points, surf_quad_points

    def partition_grid(self):
        """ Function used to locate partitioned grid file that corresponds with mpi rank of process

        Returns:
            partitioned_vertices (ndarray): array of partitioned vertex ids with shape of (n,), n
            being the number of partitioned vertices
            partitioned_coords (ndarray): array of partitioned vertex coordinates with shape of
            (n, 3)
        """
        self.logger.info("Partitioning grid")
        tau_python.tau_solver_buffer_surface()
        surface_stream = tau_python.tau_get_surface_stream_name()
        global_ids = self.tau_interface.get_int_variable(surface_stream, 'global_id')
        coord_x = self.tau_interface.get_float_variable(surface_stream, 'x')
        coord_y = self.tau_interface.get_float_variable(surface_stream, 'y')
        coord_z = self.tau_interface.get_float_variable(surface_stream, 'z')

        if global_ids is None:
            return None, None

        order = np.argsort(global_ids)
        partitioned_global_ids = global_ids[order]
        partitioned_coords = np.array([[x, y, z] for x, y, z in zip(coord_x[order], coord_y[order],
                                                                    coord_z[order])])
        return partitioned_global_ids, partitioned_coords

    def filter_surface_nodes(self, interface):
        """ Methods extracts all nodes ids and coordinates from primary grid file. The surface
        markers in the interface are used to filter all node ids and coordinates on surfaces
        of interest. Then, partitioned node ids and coordinates (only pertaining to partitioned
        domain of current process) are used to determine the partitioned node ids and coordinates
        of nodes associated with the indicates surface markers.

        Arguments:
            interface (Utility.Interface): utility.Interface jsonobject containing information on
                this preCICE data

        Returns:
            num_vertices (int): total number of nodes on surfaces of interest in partitioned
                domain
            partitioned_surface_coords (ndarray): 2D ndarray of node coordinates on surfaces of
                interest in partitioned domain of size (n x 3), where n is num_vertices
            vertex_ids (ndarray): 1D ndarray of node ids on surfaces of interest in
                partitioned domain
        """

        if self.partitioned_nids is None or self.partitioned_coords is None:
            return None, None, None
        # Identify which surfaces correspond to boundary markers
        surface_nids_indices = [True if i in interface.boundary_markers else False for i in
                                self.surf_markers]

        # Get all nodes corresponding to particular list of surface markers
        if self.surf_tri_points is not None:
            length_tri_points = self.surf_tri_points.shape[0]
            tri_points = self.surf_tri_points[surface_nids_indices[:length_tri_points]].flatten()
        else:
            length_tri_points = 0
            tri_points = np.array([])

        if self.surf_quad_points is not None:
            quad_points = self.surf_quad_points[surface_nids_indices[length_tri_points:]].flatten()
        else:
            quad_points = np.array([])
        vertex_ids = np.unique(np.hstack((tri_points, quad_points)))

        # Perform 1d intersect on partitioned_nids and blade_surface_nids to determine which
        # nodes on the blade surface are present in this partition
        vertex_ids, partitioned_surface_indices, _ = np.intersect1d(self.partitioned_nids,
                                                                    vertex_ids,
                                                                    return_indices=True)
        # Index by partitioned surface coords so that the output is ordered (will match
        # precice)
        partitioned_surface_coords = self.partitioned_coords[partitioned_surface_indices]

        if partitioned_surface_coords.size == 0:
            return None, None, None

        # Number of partitioned surface points to set up precice mesh
        num_vertices = partitioned_surface_coords.shape[0]

        return num_vertices, partitioned_surface_coords, vertex_ids
