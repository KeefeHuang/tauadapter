"""
@package tauadapter
This module contains the HelicopterSolver class that controls the execution loop of TAU when
running the
helicopter simulation. This class performs the following actions in the given order when created and
run:
1) Sets up the TAU solver based on the input parafile and sets up the datahandler.DataHandler
objects in the interface.Interface class.
2) Performs the execution loop (with or without preCICE) based on the given parafile
3) Calls interface.DataInterface class when data needs to be read or written from preCICE
"""

from os.path import join
import re

from mpi4py import MPI
import numpy as np
import precice  # pylint: disable=import-error
import tau_python  # pylint: disable=import-error
from tau_python import *  # pylint: disable=import-error, wildcard-import

from tauadapter.solver import Solver
from tauadapter.variables import Variables


class HelicopterSolver(Solver):
    """ HeliSolver class is the subclass of solver.Solver used for the rotor blade loose coupling
     case

     Attributes:
        coupling_iteration (int): number of coupling iterations with preCICE performed
        data_interface (Interface.DataInterface): interface.DataInterface object used to control
            data to and from preCICE
        rank (int): mpi rank of process as int
    """

    # Constructor for helisolver
    def __init__(self, config_file, data_interface):
        """ Constructor for subclass HeliSolver.
        Calls the constructor for the base class solver.Solver.
        Sets up logger and mpi rank. Creates folders required for simulation

        Arguments:
            config_file (str): config file name as string, parsed with
            helicopterconfig.HelicopterConfig
            data_interface (interface.DataInterface object): interface.DataInterface object used to
                control data to and from preCICE
        """
        super(HelicopterSolver, self).__init__(config_file, data_interface)
        self.simulation = self.config.simulation
        self.rank = MPI.COMM_WORLD.Get_rank()

        self.coupling_iteration = 0
        self.create_folder_structure()
        if self.config.exchange_variables:
            self.exchange_variables()

        MPI.COMM_WORLD.barrier()

        # Initialize tau_solver now to access node/vertex information for setting up data handlers
        self.logger.info("Initializing tau solver")
        self.tau_solver.init()
        tau_python.tau_parallel_sync()

    def exchange_variables(self):
        """ Method that determined what variables need to be passed to other solver and fills
                write_dict. Instantiates Variables object and passes data via written json file.
        """
        write_dict = {}
        # Get timestep per revolution data and omega value
        with open(self.config.original_motion_file, "r") as motion_file_data:
            motion_data = motion_file_data.read()
            motion_pattern = re.compile(
                r"Number of time steps per period:\s*([0-9]+)\s*")
            self.simulation.iterations_per_revolution = int(
                re.search(motion_pattern, motion_data).group(1))
        # Get simulation output period

        self.simulation.output_period = int(self.tau_para.get_para_value("Surface output period"))
        azimuth_size = np.linspace(0, 360, num=int(
            self.simulation.iterations_per_revolution / self.simulation.output_period) + 1).shape[0]
        write_dict['interfaces'] = [{'read_data': ['Azimuths0'], 'size': azimuth_size}]
        write_dict['simulation'] = {'total_azimuths': azimuth_size}
        variables = Variables(self.config, write_dict)
        variables.exchange_variables()
        for interface in self.config.interfaces:
            if interface.mesh == "TAU_Azimuths":
                interface.size = azimuth_size

    def create_folder_structure(self):
        """ Setup folder structure based on input config file"""
        # Get original motion file from parafile. Prevents infinite appending of iteration number
        with open(str(self.config.parafile), "r") as parafile:
            parafile_text = parafile.read()
            output_pattern = re.compile(r"Output files prefix:\s*(.*)\n")
            output_prefix = re.search(output_pattern, parafile_text).group(1)
            motion_pattern = re.compile(r"Motion description filename:\s*(.*)\n")
            self.config.original_motion_file = \
                re.search(motion_pattern, parafile_text).group(1)
        self.config.default_output_location = output_prefix.rsplit("/", 1)[0]
        self.config.default_output_prefix = output_prefix.rsplit("/", 1)[1]

        # Create required output folders if missing
        if self.rank == 0:
            # Setup necessary and optional folders if possible.
            necessary_folders = ['default_output_location']
            optional_folders = ['restart_location']

            for folder in necessary_folders:
                self.create_necessary_folder(folder)

            for folder in optional_folders:
                self.create_optional_folder(folder)

    # pylint: disable=method-hidden
    def tau_simulation_loop(self, num_iterations_needed):
        """ Method that runs TAU simulation loop based on number of required iterations. Simulation
            will run until tau_python.tau_solver_this_timestep == num_iterations needed.

        Arguments:
            num_iterations_needed (int): Number of TAU inner loop iterations required.
        """

        min_residual = tau_python.tau_solver_min_residual()
        inner_n = tau_python.tau_solver_inner_loop_iteration_number()

        while tau_python.tau_solver_this_timestep() < num_iterations_needed:

            self.logger.info("Starting outer loop for full multigrid")
            tau_python.tau_solver_outer_loop_init()

            tau_python.tau_solver_unsteady_advance_time()
            tau_python.tau_solver_unsteady_advance_motion()

            i = tau_python.tau_solver_this_timestep()
            self.logger.info(
                'Timestep %d in revolution %s: Advancing time and motion for unsteady '
                'simulation', i, self.coupling_iteration)

            self.logger.info('Starting full multigrid')
            tau_python.tau_solver_full_multigrid()

            # Run inner loop for number of iterations specified in parameter file
            self.logger.info('Solving inner loop')
            tau_python.tau_solver_inner_loop(0, inner_n, min_residual)

            # Print monitoring values and increment physical time step
            tau_python.tau_solver_unsteady_monitoring()

            # Write results to files
            self.logger.info('Outputting results')

            # Write field data every fieldPeriod time steps if requested or if
            # it is the last iteration (to generate restart data)
            tau_python.tau_solver_write_output_conditional()

            tau_python.tau_parallel_sync()

    def execute_precice(self):
        """ Implements abstract method solver.Solver.execute_precice(). Controls execution loop of
        main TAU simulation with preCICE.
        """

        self.logger.info('Initialize solver')

        # Initialize precice interface and tau solver
        self.logger.info('Initialize preCICE interfaces')
        self.precice_interface.initialize()

        if self.precice_interface.is_action_required(precice.action_write_initial_data()):
            self.precice_interface.fulfilled_action(precice.action_write_initial_data())

        if self.simulation.coupling_iteration:
            self.coupling_iteration = int(self.simulation.coupling_iteration)
        else:
            self.coupling_iteration = 0

        self.logger.info('Beginning simulation loop')

        # Run initial revolutions
        if self.simulation.initial_revolutions != 0:
            # Create output folder for initial revolutions
            self.config.output_location = \
                join(self.config.default_output_location, "initial_revolutions")
            if self.rank == 0:
                self.create_necessary_folder("output_location")
            tau_python.tau_parallel_sync()

            # Get values from solver to control simulation
            initial_output_prefix = join(self.config.output_location,
                                         self.config.default_output_prefix)
            self.tau_para.update(self.set_para_output_location(initial_output_prefix))

            # Run initial iterations without precice
            initial_iterations = \
                self.simulation.iterations_per_revolution * self.simulation.initial_revolutions

            self.tau_simulation_loop(initial_iterations)

        self.simulation.current_iteration = 0
        while self.precice_interface.is_coupling_ongoing():
            # Write collocation points to config file if it exists
            self.data_interface.initialize_data(self.coupling_iteration)

            if self.precice_interface.is_action_required(
                    precice.action_write_iteration_checkpoint()):
                self.logger.warning("Please implement checkpoint writing if using an Implicit "
                                    "coupling scheme")
                self.precice_interface.fulfilled_action(precice.action_write_iteration_checkpoint())

            # Create folder for new coupling revolution
            self.config.output_location = \
                join(self.config.default_output_location, "revolution{}".format(
                    self.coupling_iteration))

            if self.rank == 0:
                self.create_necessary_folder("output_location")
            tau_python.tau_parallel_sync()
            output_prefix = join(self.config.output_location, self.config.default_output_prefix)
            self.tau_para.update(self.set_para_output_location(output_prefix))

            try:
                num_revolutions_per_coupling = self.simulation.revolutions[self.coupling_iteration]
            except (IndexError, KeyError):
                self.logger.info(
                    "Number of revolutions for coupling iteration %d not found in config file, "
                    "setting by default to 2",
                    self.coupling_iteration)
                num_revolutions_per_coupling = 2

            if self.simulation.current_iteration == 0:
                previous_revolutions = 0
                for i in range(self.coupling_iteration):
                    try:
                        previous_revolutions += self.simulation.revolutions[i]
                    except IndexError:
                        previous_revolutions += 2
                self.simulation.current_iteration = \
                    self.simulation.iterations_per_revolution * (
                        previous_revolutions + self.simulation.initial_revolutions +
                        num_revolutions_per_coupling)
            else:
                self.simulation.current_iteration += self.simulation.iterations_per_revolution * \
                                                     num_revolutions_per_coupling

            self.logger.info("Running inner iterations until %d",
                             self.simulation.current_iteration)
            # Change parafile output prefix to initial revolutions folder

            self.tau_solver.finalize()
            tau_python.tau_free_prims()
            tau_python.tau_parallel_sync()
            self.tau_solver.init()
            # Get values from solver to control simulation
            self.tau_simulation_loop(self.simulation.current_iteration)

            self.data_interface.write_all(self.coupling_iteration)

            self.logger.info("Advancing time")
            self.precice_interface.advance(1)
            self.coupling_iteration += 1
            self.data_interface.read_all(self.coupling_iteration)
            if self.precice_interface.is_action_required(
                    precice.action_read_iteration_checkpoint()):  # i.e. not yet converged
                self.logger.warning("Please implement checkpoint reading if using an Implicit "
                                    "coupling scheme")
                self.precice_interface.fulfilled_action(precice.action_read_iteration_checkpoint())
            tau_python.tau_parallel_sync()
        tau_python.tau_solver_output_field()
        tau_python.tau_solver_output_surface()
        tau_python.tau_parallel_sync()
        self.tau_solver.stop()
        self.precice_interface.finalize()

    def execute(self):
        """ Implements abstract method solver.Solver.execute(). Controls execution loop of main
        TAU simulation without preCICE.
        """

        self.logger.info('Initialize solver')
        tau_python.tau_parallel_sync()

        # Get values from solver to control simulation
        min_residual = tau_python.tau_solver_min_residual()
        inner_n = tau_python.tau_solver_inner_loop_iteration_number()
        unsteady_time_steps = tau_python.tau_solver_outer_loop_iteration_number()

        if self.simulation.coupling_iteration:
            self.coupling_iteration = int(self.simulation.coupling_iteration)
        else:
            self.coupling_iteration = 0

        for _ in range(unsteady_time_steps + 1):

            self.logger.info("Starting outer loop for full multigrid")
            # Initialize outer loop with full multigrid start

            tau_python.tau_solver_outer_loop_init()

            # Advance time and motion
            self.logger.info(
                'Iteration %d: Advancing time and motion for unsteady simulation',
                self.coupling_iteration)
            tau_python.tau_solver_unsteady_advance_time()
            tau_python.tau_solver_unsteady_advance_motion()

            # Start full multigrid
            self.logger.info('Starting full multigrid')
            tau_python.tau_solver_full_multigrid()

            # Run inner loop for number of iterations specified in parameter file
            self.logger.info('Solving inner loop')
            tau_python.tau_solver_inner_loop(0, inner_n, min_residual)

            # Print monitoring values and increment physical time step
            tau_python.tau_solver_unsteady_monitoring()

            # Write results to files
            self.logger.info('Outputting results')

            # Write field data every fieldPeriod time steps if requested or if
            # it is the last iteration (to generate restart data)
            tau_python.tau_solver_write_output_conditional()

            # Finalize monitor history file (removes .tmp from file name)
            # tau_solver_output_monitor()

            if self.rank == 0 and self.coupling_iteration % self.simulation.output_period == 0:
                self.tau2plt(str(self.config.parafile))

            tau_python.tau_parallel_sync()

            self.coupling_iteration += self.simulation.output_period

            tau_python.tau_parallel_sync()

        tau_python.tau_solver_output_field()
        tau_python.tau_solver_output_surface()
        tau_python.tau_parallel_sync()

        self.tau_solver.stop()
        self.tau_solver.finalize()
