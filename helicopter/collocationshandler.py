"""
@package tauadapter
This module contains the CollocationsHandler class. This class is a subclass of the
datahandler.DataHandler class used for the HelicopterSolver case and handles collocation data passed
from preCICE and stores processed data in the utility.Config object shared by all DataHandler
objects.
"""

import logging
from mpi4py import MPI
from tauadapter.datahandler import DataHandler


class CollocationsHandler(DataHandler):
    """ CollocationsHandler class. Handles collocation data from preCICE.

    Attributes:
        comm (MPI object): MPI communication handle used to call MPI functions
        logger (logging): logger object handle used to log events
    """

    def __init__(self, args):
        """ Constructor for CollocationsHandler class.

        Arguments:
            args (dict): dictionary of datahandler arguments to be parsed by base class
        """
        super(CollocationsHandler, self).__init__(args)

        self.comm = MPI.COMM_WORLD
        self.logger = logging.getLogger("tau" + str(self.comm.Get_rank()) + ".CollocationHandler")

    def read(self, data, iteration):
        """ Implements datahandlers.DataHandler.read() abstract function. Receives Collocation data
        from preCICE and stores data in self.config.

        Arguments:
            data (ndarray): data received from preCICE. This is in a 1D array of size (
                self.length). stores collocation data from solid solver
            iteration (int): current coupled iteration as int
        """
        self.logger.debug("Passed collocation data is %s", str(data))

        self.simulation.collocations = list(data)

    def write(self, iteration):
        """ Implements datahandlers.DataHandler.write() abstract function. Write is not implemented
        for collocation data in FlapSolver

        Arguments:
            iteration (int): current coupled iteration as int
        """
        self.logger.error("Write is not implemented for Displacements for this module\n")
        raise NotImplementedError

    def is_scalar(self):
        """
        Returns:
             Returns true as this is a scalar data type
        """
        return True
