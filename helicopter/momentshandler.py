"""
@package tauadapter
This module contains the MomentsHandler class. This class is a subclass of the
datahandler.DataHandler class used for the HelicopterSolver case and handles moment data passed
to preCICE. Raw force and coordinate data is read from TAU output files. The data is rotated into
the blade-local frame and the blade-local moment arms are calculated for each node. Blade-local
moment is then calculated and moment data is passed to preCICE.
"""

import logging
import re
from os import listdir
from os.path import join
import sys

import numpy as np
from mpi4py import MPI
import tau_python  # pylint: disable=import-error

from tauadapter.datahandler import DataHandler
from tauadapter.utility import read_grid, rotation_matrix


# pylint: disable=too-many-instance-attributes
class MomentsHandler(DataHandler):
    """ ForcesMomentsHandler class. Handles force data from TAU and writes data to preCICE

    Attributes:
        collocation_point_ids (ndarray): 1D array of size (n), where n is the number of nodes
            handled by this DataHandler. Contains a list of panel numbers for each node in
            DataHandler, arranged per precice_ids.
        collocation_points (ndarray): Array of size (c x 3) where c is the number of collocation
            points. Contains collocation point coordinates in blade-local coordinate system.
        length (int): number of nodes on interface (n) for current process
        logger (logging): logger object handle used to log events
        pattern_prefix (string): output file prefix
        parallel_pattern_suffix (string): output file suffix for parallel execution
        serial_pattern_suffix (string): output file suffix for serial execution
        """

    def __init__(self, args):
        """ Constructor for MomentsHandler class.

        Arguments:
            args: tuple of multiple values, see member attributes for more information. contains
             (vertex_ids, coordinates, utility.Config instance, utility.Interface instance).
        """
        super(MomentsHandler, self).__init__(args)
        # Get global IDs of all nodes handled by ForceMomentsHandler ordered per precice_ids

        comm = MPI.COMM_WORLD

        self.azimuth_position = self.interface.azimuth
        self.logger = logging.getLogger(
            "tau" + str(comm.Get_rank()) + ".MomentsHandler" + str(self.azimuth_position))

        # Number of nodes handled by ForceHandler
        self.length = len(self.precice_vertex_ids)

        # Prefix to search for correct output file
        self.pattern_prefix = ".*surface.*i="
        rank = tau_python.tau_mpi_rank()
        # Adds domain flag suffix in case of parallel case
        if tau_python.tau_mpi_nranks() > 1:
            self.parallel_pattern_suffix = r"_.*domain_" + str(rank) + r"(?=\n)"

        self.serial_pattern_suffix = r"_.*e[\-0-9]+(?=\n)"

        self.r_edges = None
        self.collocation_points = None
        self.collocation_point_ids = None
        self.iteration_modifier = None
        self.current_blade = self.interface.blade
        self.set_iteration_modifier(self.azimuth_position)
        self.logger.debug("Output iteration modifier is %f", self.iteration_modifier)

    def set_iteration_modifier(self, azimuth_position):
        """ Method that sets the member attribute iteration modifier. This identifies which
        TAU output file to read.

        Arguments:
            azimuth_position (int): azimuth position number out of total azimuths
        """

        assert azimuth_position <= self.simulation.total_azimuths, "Azimuth position must always " \
                                                                   "be less than total number of " \
                                                                   "azimuths"

        self.logger.debug("Azimuth position of logger is %d", azimuth_position)
        output_files_per_azimuth_position = self.simulation.iterations_per_revolution / \
                                        self.simulation.total_azimuths
        #  Determine modifier to find relevant output file
        # For piece-wise interpolation, all blades are seen as a single blade. Thus, each output
        # file gives b (# blades) time steps of data. Output files per blade is then total azimuths
        # / #blades. The output file for this DataHandler is then azimuth position % output files
        # per blade + 1
        if self.simulation.interpolation_type == "piece-wise":
            output_files_per_blade = self.simulation.total_azimuths / self.simulation.blades
            self.logger.debug("Output file per blade is %d", output_files_per_blade)
            if azimuth_position == 0:
                self.iteration_modifier = - output_files_per_blade * \
                                          output_files_per_azimuth_position
            else:
                self.iteration_modifier = - ((output_files_per_blade - azimuth_position %
                                              output_files_per_blade) % output_files_per_blade *
                                             output_files_per_azimuth_position)
        # For continuous interpolation, forces/moments of all blades are used. Thus, each output
        # file gives 1 time steps of data. Output files per blade is then total azimuths. The output
        # file for this DataHandler is then azimuth position + 1
        else:
            output_files_per_blade = self.simulation.total_azimuths
            self.iteration_modifier = - (output_files_per_blade - azimuth_position) * \
                                      output_files_per_azimuth_position

    def write(self, _iteration):
        """ Implements datahandlers.DataHandler.write() abstract function. Writes moment
         data to the preCICE interface

        Arguments:
            _iteration (int): current coupled iteration as int

        Returns:
            data: 1D array of size (3*self.length). passes force data arranged as [mx1, my1, mz1,
                mx2, my2, mz2, ...]
        """
        if self.collocation_points is None:
            self.set_collocation_points()
        # Get list of all output files
        files = "\n".join(listdir(self.config.output_location)) + "\n"

        # Determine which iteration of output is required.
        current_iteration = self.simulation.current_iteration + self.iteration_modifier

        self.logger.debug("Current iteration from datahandler is %s", current_iteration)
        # Creates file pattern to search for output file
        try:
            text_pattern = \
                self.pattern_prefix + str(current_iteration) + self.serial_pattern_suffix
            iteration_pattern = re.compile(
                self.pattern_prefix + str(current_iteration) + self.serial_pattern_suffix)
            self.logger.debug("Current iteration pattern is %s", text_pattern)

            output_file = re.search(iteration_pattern, files).group(0)
            self.logger.debug("Found output file is %s", output_file)
        except AttributeError:
            text_pattern = self.pattern_prefix + str(current_iteration) + \
                           self.parallel_pattern_suffix
            iteration_pattern = re.compile(
                self.pattern_prefix + str(current_iteration) + self.parallel_pattern_suffix)
            self.logger.debug("Current iteration pattern is %s", text_pattern)

            output_file = re.search(iteration_pattern, files).group(0)
            self.logger.debug("Found output file is %s", output_file)

        # Get all data from output file
        force_data, coord_data = self.process_data(join(self.config.output_location, output_file))

        collocation_points = self.collocation_points[self.collocation_point_ids]

        moment_arms = coord_data - collocation_points
        moment_data = np.cross(moment_arms, force_data)

        return moment_data.flatten()

    def read(self, _data, _iteration):
        """ Reads moment data from preCICE. This is not implemented for the HelicopterSolver
         case.

        Arguments:
            _data (ndarray): data received from preCICE. This is in a 1D array of size (n x 3)
            _iteration (int): current coupled iteration as int
        """

        self.logger.error("Read is not implemented for Forces/Moments for this module\n")
        raise NotImplementedError

    def is_scalar(self):
        """
        Returns:
             Returns false as this is a vector data type
        """
        return False

    def process_data(self, output_file):
        """ Function reads the name of a TAU output file. Extracts all force data, filters for
        vertices in this partition and arranges it to match the order of self.precice_vertex_ids

        Arguments:
            output_file (string): name of TAU output file as a str

        Return:
            data (ndarray): 2D array of force data arranged as [[x0, y0, z0], [x1, y1, z1],
            [x2, y2, z2] ...] size (n x 3) where n is the number of nodes
        """

        # Read netcdf file
        try:
            data_dict = read_grid(output_file)
        except IOError:
            self.logger.exception("Output file %s not found", output_file)
            sys.exit(1)

        # Pull global id from netcdf file
        data_nodes = data_dict['global_id']
        # Determine which nodes belong to this MPI rank
        _, comm1, _ = np.intersect1d(data_nodes, self.vertex_ids,
                                     return_indices=True)

        force_x_data = data_dict['x-force'][comm1]
        force_y_data = data_dict['y-force'][comm1]
        force_z_data = data_dict['z-force'][comm1]

        coord_x_data = data_dict['x'][comm1]
        coord_y_data = data_dict['y'][comm1]
        coord_z_data = data_dict['z'][comm1]

        # Arrange data in format [x0, y0, z0, x1, y1, z1, x2, y2, z2 ...]
        force_buffer = np.array(zip(force_x_data, force_y_data, force_z_data))
        coord_buffer = np.array(zip(coord_x_data, coord_y_data, coord_z_data))

        blade_angle_spacing = 360.0 / self.simulation.blades
        force_buffer = rotation_matrix(-blade_angle_spacing * self.current_blade, 3,
                                       dim='deg').dot(force_buffer.T).T
        coord_buffer = rotation_matrix(-blade_angle_spacing * self.current_blade, 3,
                                       dim='deg').dot(coord_buffer.T).T

        return force_buffer, coord_buffer

    def set_collocation_points(self):
        """ Function that takes in collocation point data from the preCICE and determines the panel
        each node handled by the DataHandler belongs to. This data is written to list
        self.collocation_point_ids the first time set_collocation_points is called
        """
        collocation_data = self.simulation.collocations

        num_points = self.simulation.num_collocation_points
        self.r_edges = np.array(collocation_data[:num_points + 1])
        self.collocation_points = np.array(collocation_data[num_points + 1:num_points * 4 + 1])
        assert self.collocation_points.shape[0] % 3 == 0, "The number of collocation points must " \
                                                          "be divisible by three"
        self.collocation_points = self.collocation_points.reshape(-1, 3)

        coordinates = np.reshape(self.coordinates, (-1, 3))

        if self.collocation_point_ids is None:
            self.collocation_point_ids = np.zeros(self.length)
            node_y_coordinates = coordinates[:, 1]
            for i in range(num_points):
                y_0 = self.r_edges[i]
                y_1 = self.r_edges[i + 1]
                node_gr_index = np.array(node_y_coordinates >= y_0)
                node_ls_index = np.array(node_y_coordinates < y_1)
                indexes = np.logical_and(node_gr_index,  # pylint: disable=assignment-from-no-return
                                         node_ls_index)  # pylint: disable=assignment-from-no-return
                self.collocation_point_ids[indexes] = int(i)

            indexes = np.where(node_y_coordinates == self.collocation_points[-1])
            self.collocation_point_ids[indexes] = num_points - 1
            self.collocation_point_ids = self.collocation_point_ids.astype(int)
