"""
@package tauadapter
This module contains the MotionsHandler class. This class is a subclass of the
datahandler.DataHandler class used for the HelicopterSolver case and handles motion data passed
from preCICE. Motion data is passed as a series of fourier coefficients for flap, lag, pitch,
axial motion. Note that the passed data is always in degrees. The MotionsHandler class will
update the TAU motion file controlling the rigid body motion of the blade during simulation and
will save all fourier coefficients in the config.Config object shared by all DataHandler objects
to pass displacement deltas if needed.
"""
import re
import sys
import logging
import math
import numpy as np
from mpi4py import MPI

from tauadapter.datahandler import DataHandler


class MotionsHandler(DataHandler):
    """ MotionsHandler class. Handles motion data as Fourier series from preCICE.

    Attributes:
        blades (int): number of blades in simulation as int
        comm (MPI.COMM_WORLD): MPI communication handle used to call MPI functions
        file_name (string): new motion file is generated for each revolution. this is the location
            of latest motion file
        frequency (float): frequency of rotation of TAU rotorblades
        logger (logging): logger object handle used to log events
    """

    def __init__(self, args):
        """ Constructor for MotionsHandler class.

        Arguments:
            args (tuple): tuple of multiple values, see member attributes for more information.
                Contains (blades).
        """

        super(MotionsHandler, self).__init__(args)

        self.comm = MPI.COMM_WORLD
        self.logger = logging.getLogger(
            "tau" + str(self.comm.Get_rank()) + ".MotionsHandler")
        # File name of writen motion file, defined during runtime
        self.file_name = None
        self.frequency = None
        self.blades = self.simulation.blades

    def get_frequency(self, parafile, omega):
        """ Get frequency value from parafile. Sets member attribute frequency

        Arguments:
            parafile (tau_python.PyPara): PyPara object
            omega (float): rotational velocity in radians
        """
        gas_gamma = 1.4
        gas_r = 287

        length_pattern = re.compile(r"Hinge - reduced frequency reference length\s*:\s*([0-9]+)")
        # time_steps_pattern = re.compile(r"Number of time steps per period\s*:\s*([0-9]+)")
        with open(self.config.original_motion_file, "r") as motion_file_data:
            motion_text = motion_file_data.read()
            try:
                reference_length = re.search(length_pattern, motion_text).group(1)
            except AttributeError:
                self.logger.exception("Reference length cannot be found", exc_info=1)
                sys.exit()

        try:
            revolution_velocity = float(parafile.get_para_value("Reference velocity"))
        except ValueError:
            try:
                self.logger.info(
                    "Unable to find reference velocity, calculating based off temperature and Mach "
                    "number")
                reference_temperature = parafile.get_para_value("Reference temperature")
                reference_mach_number = parafile.get_para_value("Reference Mach number")
                revolution_velocity = reference_mach_number * np.sqrt(
                    gas_gamma * gas_r * reference_temperature)
            except TypeError:
                self.logger.exception(
                    "Unable to find reference velocity, temperature, mach number variables in "
                    "parafile")
                sys.exit()

        self.frequency = float(omega) * float(reference_length) / float(revolution_velocity)

    def write(self, iteration):
        """ Implements datahandlers.DataHandler.write() abstract function. Write is not implemented
        for Motion data in HelicopterSolver

        Arguments:
            iteration (int): current coupled iteration as int
        """
        self.logger.error("Write is not implemented for Motion for this module\n")
        raise NotImplementedError

    def read(self, data, iteration):
        """ Implements datahandlers.DataHandler.read() abstract function. Reads data from precice
        and writes data to motion file. Also stores Fourier coefficients for deformations in
        config.Config object.

        Arguments:
            data (ndarray): data received from preCICE. 1D ndarray of size (self.length). stores
            angular velocity and fourier coefficients for motion of blade
            iteration (int): current coupling iteration as int. used for naming output files

        Notes:
            Packaged motion point data. Data is arranged as follows  [rpos1 coefficients,
            rpos2 coefficients] + .....
            E.g. [rpos1_flap1, rpos1_flap2, rpos1_flap3, rpos1_lag2, ...,
            , rpos_last_flap1, ... rpos_last_axial3]
        """
        self.logger.debug("Data shape %s", data.shape)
        self.logger.debug(data)
        omega = self.simulation.omega
        num_coefficients = self.simulation.num_harmonics * 2 + 1
        sensor_positions = self.simulation.rpos
        max_value = len(sensor_positions) - 1
        start_index = max_value * 4 * num_coefficients
        self.logger.debug("Starting index %s", start_index)
        # num_coefficients = int(data[1])

        # Find harmonics for maximum sensor values
        # sensor_positions = data[2::(num_coefficients * 4 + 1)]
        # max_value = max(sensor_positions)
        # rpos = np.where(sensor_positions == max_value)[0][0]
        # start_index = int(3 + rpos * (4 * num_coefficients + 1))
        flap = data[start_index: start_index + num_coefficients]
        lag = data[start_index + num_coefficients: start_index + num_coefficients * 2]
        pitch = data[start_index + num_coefficients * 2: start_index + num_coefficients * 3]
        # axial = data[start_index + num_harmonics * 3: start_index + num_harmonics * 4]

        coefficients = (flap, lag, pitch)
        self.get_frequency(self.tau_para, omega)
        self.simulation.coefficients = list(data)
        if self.comm.Get_rank() == 0:
            self.write_motion_file(coefficients, iteration, num_coefficients)
        new_motion_file = self.config.original_motion_file + str(iteration)
        para_list = {'Motion description filename': new_motion_file}
        self.tau_para.update(para_list)
        para_list = {'Motion hierarchy filename': new_motion_file}
        self.tau_para.update(para_list)

    def is_scalar(self):
        """
        Returns:
             Returns true as this is a scalar data type
        """
        return True

    def write_motion_file(self, coefficients, iteration, num_coefficients):
        """ Receives coeffs from the precice interface and writes to motion file. This method
        assumes that the order of blades in the motion file is in order: blade 1, 2, 3, 4.

        Arguments:
            coefficients (ndarray): data received from preCICE. Data is arranged as follows  [
                angular frequency of blade] + [num_harmonics] + [rpos1] + [rpos1 coefficients] +
                [rpo2] + [rpos2 coefficients] +.....
                E.g. [omega, 3, rpos1, rpos1_flap1, rpos1_flap2, rpos1_flap3, rpos1_lag2, ..., rpos_
                last, rpos_last_flap1, ... rpos_last_axial3]
            iteration (int): current coupling iteration
            num_coefficients (int): number of coefficients per fourier series

        """
        self.file_name = self.config.original_motion_file + str(iteration)
        sine_pattern = re.compile(
            r"Hinge - Fourier coefficients for rotation \(sin\)\s*:([\s0-9\.-]+)\n")
        cosine_pattern = re.compile(
            r"Hinge - Fourier coefficients for rotation \(cos\)\s*:([\s0-9\.-]+)\n")
        frequency_pattern = re.compile(r"Hinge - reduced frequency for rotation\s*:([\s0-9\.]+)\n")

        cosine_text, end_text, frequency_text, sine_text = self.extract_motion_file_text(
            cosine_pattern, frequency_pattern, sine_pattern)

        new_motion_file_data = ''
        iteration = 0
        # Determine angle between blades
        blade_spacing_angle = 2 * math.pi / self.blades
        num_harmonics = (num_coefficients - 1) // 2

        for blade_num in range(self.blades):
            # Shift the angles depending on the blade position
            # Calculate new coefficients based on the geometric relationships
            # cos(a - b) and sin(a - b)
            shift = blade_num * blade_spacing_angle
            for axis, _ in enumerate(coefficients):
                cosine = np.zeros(num_harmonics + 1)
                sine = np.zeros(num_harmonics + 1)
                cosine[0] = coefficients[axis][0]

                original_cosine_coefficients = coefficients[axis][1::2]
                original_sine_coefficients = coefficients[axis][2::2]

                cosine[1:] = np.multiply(original_cosine_coefficients, np.cos(shift)) + \
                             np.multiply(original_sine_coefficients, np.sin(shift))

                sine[1:] = np.multiply(original_cosine_coefficients, -np.sin(shift)) + \
                           np.multiply(original_sine_coefficients, np.cos(shift))

                cosine = np.array([self.round_sig(x, 5) for x in cosine])
                sine = np.array([self.round_sig(x, 5) for x in sine])

                sine = " ".join([str(x) for x in sine])
                cosine = " ".join([str(x) for x in cosine])

                new_motion_file_data += frequency_text[iteration] + " " + str(self.frequency)
                new_motion_file_data += cosine_text[iteration] + " " + cosine
                new_motion_file_data += sine_text[iteration] + " " + sine

                iteration += 1
        new_motion_file_data += end_text
        with open(self.file_name, "w") as base_file:
            base_file.write(new_motion_file_data)

    def extract_motion_file_text(self, cosine_pattern, frequency_pattern, sine_pattern):
        """ Used to extract motion file text. Writing of motion file is done by piecing together
        chunks of the original motion file while replacing the fourier series coefficients with
        data passed from precice. Motion file order is as below and stored text is as follows:
        * frequency_text[0] *
        Hinge - reduced frequency for rotation: [frequency]
        * cosine_text[0] *
        Hinge - Fourier coefficients for rotation (cos): [cosine coefficients]
        * sine_text[0] *
        Hinge - Fourier coefficients for rotation (sin): [sine coefficients]
        ...
        * sine_text[end] *
        Hinge - Fourier coefficients for rotation (sin): [sine coefficients]
        * end_text *

        Arguments:
            cosine pattern (_sre.SRE_Pattern): regular expression object used to find cosine
            coefficient locations
            frequency pattern (_sre.SRE_Pattern): regular expression object used to find cosine
            coefficient locations
            sine_pattern pattern (_sre.SRE_Pattern): regular expression object used to find cosine
            coefficient locations

        Returns:
            cosine_text (list): list of cosine text as defined in the method docstring
            frequency_text (list): list of frequency text as defined in the method docstring
            sine_text (list): list of sine text as defined in the method docstring
            end_text (str): string containing text after the last sine_text instance
        """
        with open(self.config.original_motion_file) as base_file:
            motion_file_data = base_file.read()
            sine_locations = re.finditer(sine_pattern, motion_file_data)
            cosine_locations = re.finditer(cosine_pattern, motion_file_data)
            frequency_locations = re.finditer(frequency_pattern, motion_file_data)
        sine_text = []
        cosine_text = []
        frequency_text = []
        location = 0
        # Gather all the text in the motion file other than the sine/cosine/frequency data.
        # ex. sine_location contains all the text after the previous frequency data to just before
        # the coefficients of the next sine fourier series are defined.
        for sine_match, cosine_match, frequency_match in zip(sine_locations, cosine_locations,
                                                             frequency_locations):
            frequency_location = [frequency_match.start(1), frequency_match.end(1)]
            sine_location = [sine_match.start(1), sine_match.end(1)]
            cosine_location = [cosine_match.start(1), cosine_match.end(1)]

            frequency_text.append(motion_file_data[location:frequency_location[0]])
            cosine_text.append(motion_file_data[frequency_location[1]:cosine_location[0]])
            sine_text.append(motion_file_data[cosine_location[1]:sine_location[0]])
            location = sine_location[1]
        end_text = motion_file_data[location:]
        return cosine_text, end_text, frequency_text, sine_text

    @staticmethod
    def round_sig(num, sig=2):
        """ Static method to round to certain sig figs.
        Arguments:
            num (float): float value to be rounded
            sig (int): number of sig figs. default is 2

        Returns:
            num rounded to `sig` significant figures
        """
        if num == 0:
            return 0
        return round(num, sig - int(math.floor(math.log10(abs(num)))) - 1)
