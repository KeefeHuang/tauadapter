"""
@package tauadapter
This module contains the AzimuthsHandler class. This class is a subclass of the
datahandler.DataHandler class used for the HelicopterSolver case and handles Azimuth data passed to
preCICE.
"""
import logging
import numpy as np
from mpi4py import MPI

from tauadapter.datahandler import DataHandler


class AzimuthsHandler(DataHandler):
    """ AzimuthsHandler class. Handles azimuths data to/from preCICE.

    Attributes:
        comm (mpi4py.MPI): MPI communication handle used to call MPI functions
            input config file.
        logger (logging): logger object handle used to log events
    """

    def __init__(self, args):
        """ Constructor for AzimuthsHandler class.

        Arguments:
            args (dict): dictionary of datahandler arguments to be parsed by base class
        """
        super(AzimuthsHandler, self).__init__(args)

        self.comm = MPI.COMM_WORLD
        self.logger = logging.getLogger("tau" + str(self.comm.Get_rank()) + ".AzimuthHandler")

    def read(self, _data, iteration):
        """ Implements datahandlers.DataHandler.read() abstract function. Read is not implemented
        for azimuths in HelicopterSolver

        Arguments:
            _data (ndarray): data received from preCICE. This is in a 1D array of size (self.length)
            iteration (int): current coupled iteration as int
        """
        self.logger.error("Read is not implemented for Azimuth data in HelicopterSolver\n")
        raise NotImplementedError

    def write(self, iteration):
        """ Implements datahandlers.DataHandler.write() abstract function. Writes Azimuth data to
            preCICE

        Arguments:
            iteration (int): current coupled iteration as int

        Returns:
            data (ndarray): 1D ndarray of size (self.length). contains Azimuth data.
        """
        self.simulation.azimuths = list(np.linspace(0, 360, num=int(
            self.simulation.iterations_per_revolution/ self.simulation.output_period) + 1))
        data = np.array(self.simulation.azimuths)

        return data

    def is_scalar(self):
        """
        Returns:
             Returns true as this is a scalar data type
        """
        return True
