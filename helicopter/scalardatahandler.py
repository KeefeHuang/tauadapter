"""
@package tauadapter
This module contains the ForcesHandler class. This class is a subclass of the
datahandler.DataHandler class and handles force and moment data passed to preCICE. Raw force and
coordinate data is read from TAU output files. The data is rotated into the blade-local frame and
the blade-local moment arms are calculated for each node. Blade-local moment is then calculated and
force and moment data is passed to preCICE.
"""

import logging
import re
import sys
from os import listdir
from os.path import join

import numpy as np
from mpi4py import MPI
import tau_python  # pylint: disable=import-error

from tauadapter.datahandler import DataHandler
from tauadapter.utility import read_grid, rotation_matrix
import tauadapter.constants as constant


# pylint: disable=too-many-instance-attributes
class ScalarHandler(DataHandler):
    """ ForcesMomentsHandler class. Handles force data from TAU and writes data to preCICE

    Attributes:
        logger (logging): logger object handle used to log events
        pattern_prefix (string): output file prefix
        parallel_pattern_suffix (string): output file suffix for parallel execution
        serial_pattern_suffix (string): output file suffix for serial execution
        """

    def __init__(self, args):
        """ Constructor for ForcesHandler class.

        Arguments:
            args: tuple of multiple values, see member attributes for more information. contains
             (vertex_ids, coordinates, utility.Config instance, utility.Interface instance).
        """
        super(ScalarHandler, self).__init__(args)
        # Get global IDs of all nodes handled by ForceMomentsHandler ordered per precice_ids
        comm = MPI.COMM_WORLD

        self.azimuth_position = self.interface.azimuth
        self.logger = logging.getLogger(
            "tau" + str(comm.Get_rank()) + ".ForcesHandler" + str(self.azimuth_position))

        # Prefix to search for correct output file
        self.pattern_prefix = ".*surface.*i="
        rank = tau_python.tau_mpi_rank()

        # Adds domain flag suffix in case of parallel case
        if tau_python.tau_mpi_nranks() > 1:
            self.parallel_pattern_suffix = r"_.*domain_" + str(rank) + r"(?=\n)"

        self.serial_pattern_suffix = r"_.*e[\-0-9]+(?=\n)"

        self.iteration_modifier = None
        self.current_blade = self.interface.blade
        self.set_iteration_modifier(self.azimuth_position)
        self.logger.debug("Output iteration modifier is %f", self.iteration_modifier)

        self.data_types = args[constant.DATA_TYPE]

    def set_iteration_modifier(self, azimuth_position):
        """ Method that sets the member attribute iteration modifier. This is the angle

        Arguments:
            azimuth_position (int): azimuth position number out of total azimuths

        """

        assert azimuth_position <= self.simulation.total_azimuths, "Azimuth position must always " \
                                                                   "be less than total number of " \
                                                                   "azimuths"

        self.logger.debug("Azimuth position of logger is %d", azimuth_position)
        output_files_per_azimuth_position = self.simulation.iterations_per_revolution / \
                                            self.simulation.total_azimuths
        #  Determine modifier to find relevant output file
        # For piece-wise interpolation, all blades are seen as a single blade. Thus, each output
        # file gives b (# blades) time steps of data. Output files per blade is then total azimuths
        # / #blades. The output file for this DataHandler is then azimuth position % output files
        # per blade + 1
        if self.simulation.interpolation_type == "piece-wise":
            output_files_per_blade = self.simulation.total_azimuths / self.simulation.blades
            self.logger.debug("Output file per blade is %d", output_files_per_blade)
            if azimuth_position == 0:
                self.iteration_modifier = - output_files_per_blade * \
                                          output_files_per_azimuth_position
            else:
                self.iteration_modifier = - ((output_files_per_blade - azimuth_position %
                                              output_files_per_blade) % output_files_per_blade *
                                             output_files_per_azimuth_position)
        # For continuous interpolation, forces/moments of all blades are used. Thus, each output
        # file gives 1 time steps of data. Output files per blade is then total azimuths. The output
        # file for this DataHandler is then azimuth position + 1
        else:
            output_files_per_blade = self.simulation.total_azimuths
            self.iteration_modifier = - (output_files_per_blade - azimuth_position) * \
                                      output_files_per_azimuth_position

    def write(self, _):
        """ Implements datahandlers.DataHandler.write() abstract function. Writes force and moment
         data to the precice interface

        Arguments:
            iteration (int): current coupled iteration as int

        Returns:
            data: tuple of 1D array of force data arranged as [fx1, fy1, fz1, fx2, fy2, fz2, ...]
              and moment data arranged as [mx1, my1, mz1, mx2, my2, mz2, ...]
        """
        # Get list of all output files
        files = "\n".join(listdir(self.config.output_location)) + "\n"

        # Determine which iteration of output is required.
        current_iteration = self.simulation.current_iteration + self.iteration_modifier

        self.logger.debug("Current iteration from datahandler is %s", current_iteration)
        # Creates file pattern to search for output file
        try:
            text_pattern = \
                self.pattern_prefix + str(current_iteration) + self.serial_pattern_suffix
            iteration_pattern = re.compile(
                self.pattern_prefix + str(current_iteration) + self.serial_pattern_suffix)
            self.logger.debug("Current iteration pattern is %s", text_pattern)

            output_file = re.search(iteration_pattern, files).group(0)
            self.logger.debug("Found output file is %s", output_file)
        except AttributeError:
            text_pattern = self.pattern_prefix + str(current_iteration) + \
                           self.parallel_pattern_suffix
            iteration_pattern = re.compile(
                self.pattern_prefix + str(current_iteration) + self.parallel_pattern_suffix)
            self.logger.debug("Current iteration pattern is %s", text_pattern)

            output_file = re.search(iteration_pattern, files).group(0)
            self.logger.debug("Found output file is %s", output_file)

        # Get all data from output file
        force_data = self.process_data(join(self.config.output_location, output_file))

        return force_data.flatten()

    def read(self, _data, _iteration):
        """ Reads force data from preCICE. This is not implemented for force data for the HeliSolver
         case.

        Arguments:
            _data (ndarray): data received from preCICE. This is in a 1D array of size (n x 3)
            _iteration (int): current coupled iteration as int
        """

        self.logger.error("Read is not implemented for Forces/Moments for this module\n")
        raise NotImplementedError

    def is_scalar(self):
        """
        Returns:
             Returns false as this is a vector data type
        """
        return False

    def process_data(self, output_file):
        """ Function reads the name of a TAU output file. Extracts all force data, filters for
        vertices in this partition and arranges it to match the order of self.precice_vertex_ids

        Arguments:
            output_file (string): name of TAU output file as a str

        Return:
            data (ndarray): 2D array of force data arranged as [[x0, y0, z0], [x1, y1, z1],
            [x2, y2, z2] ...] size (n x 3) where n is the number of nodes
        """

        # Read netcdf file
        try:
            data_dict = read_grid(output_file)
        except IOError:
            self.logger.exception("Output file %s not found", output_file)
            sys.exit()

        # Pull global id from netcdf file
        data_nodes = data_dict['global_id']
        # Determine which nodes belong to this MPI rank
        _, comm1, _ = np.intersect1d(data_nodes, self.vertex_ids,
                                     return_indices=True)

        force_x_data = data_dict['x-force'][comm1]
        force_y_data = data_dict['y-force'][comm1]
        force_z_data = data_dict['z-force'][comm1]

        # Arrange data in format [x0, y0, z0, x1, y1, z1, x2, y2, z2 ...]
        force_buffer = np.array(zip(force_x_data, force_y_data, force_z_data))

        blade_angle_spacing = 360.0 / self.simulation.blades
        force_buffer = rotation_matrix(-blade_angle_spacing * self.current_blade, 3,
                                       dim='deg').dot(force_buffer.T).T
        return force_buffer
