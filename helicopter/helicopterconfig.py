"""
@package tauadapter
This module contains the HelicopterConfig, Interface, Simulation classes. These classes are used
to store input data during the coupled Simulation.

HelicopterConfig:
    subclassed from tauadapter.Config class. Adds new Simulation and new Interface
    classes

HelicopterInterface:
    subclassed from tauadapter.Interface class. Adds new member attributes
    vertex_ids, blade, azimuth used by HelicopterSolver DataHandlers

Simulation:
    new jsonobject class. stores various data read/calculated during the coupled simulation
"""

import jsonobject
from tauadapter.config import Interface, Config


class Simulation(jsonobject.JsonObject):
    """ Simulation class represents a jsonobject that stores the data needed to configure the
    TAU simulation.

    Attributes:
        coupling_iteration (int): current coupled iteration number as int. used to set starting
        iteration if restarting

        total_azimuths (int): total number of azimuths in simulation as int
            blades (int): number of blades in simulation as int

        iterations_per_revolution (int): number of timesteps per revolution. variable will be set
            automatically. used to pass data between various methods.

        azimuths (list): list of azimuth positions as angles from starting position, length
            of total_azimuths. Used internally to pass information between datahandlers.

        collocations (list): list of collocation point location data. Used internally to pass
            information between datahandlers
        coefficients (list): list of motion data as fourier series. Used internall to pass
            information between datahandlers
        interpolation_type (str): type of interpolation used as str
    """

    total_azimuths = jsonobject.IntegerProperty(required=True)

    num_collocation_points = jsonobject.IntegerProperty()
    rpos = jsonobject.ListProperty(float)
    redge = jsonobject.ListProperty(float)
    num_harmonics = jsonobject.IntegerProperty()
    omega = jsonobject.FloatProperty()

    coupling_iteration = jsonobject.IntegerProperty(default=0)
    blades = jsonobject.IntegerProperty(required=True)
    rotation_order = jsonobject.StringProperty()

    iterations_per_revolution = jsonobject.IntegerProperty()

    azimuths = jsonobject.ListProperty(float)
    collocations = jsonobject.ListProperty(float)
    coefficients = jsonobject.ListProperty(float)

    interpolation_type = jsonobject.StringProperty()


class HelicopterInterface(Interface):
    """ Subclassed Interface class adds in HelicopterSolver specific information

    Attributes:
        vertex_ids (list): list of vertex ids related to boundary markers
        blade (int): blade number of boundary markers as int
        azimuth (int): azimuth position of data as int
    """

    vertex_ids = jsonobject.ListProperty(list)
    blade = jsonobject.IntegerProperty()
    azimuth = jsonobject.IntegerProperty()


class HelicopterConfig(Config):
    """ HelicopterConfig class represents a jsonobject that stores the data needed to configure the
    tau-python precice interface for Helicopter simulations.

    Attributes:
        simulation (Simulation): jsonobject that stores data required for helicopter simulation
        deformation (Deformation): jsonobject that stores data required for helicopter deformation
        interfaces (list): list of HelicopterInterface class objects used in the simulation
    """

    simulation = jsonobject.ObjectProperty(Simulation)
    interfaces = jsonobject.ListProperty(jsonobject.ObjectProperty(HelicopterInterface))
