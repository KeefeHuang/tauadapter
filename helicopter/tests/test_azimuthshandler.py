"""
@package tauadapter
This module contains the unittests for the azimuths data handler for the HelicopterSolver
"""
import unittest
from mock import Mock, patch

import numpy as np
from helicopter.azimuthshandler import AzimuthsHandler


# pylint: disable=arguments-differ, unused-argument
class AzimuthsHandlerTests(unittest.TestCase):
    """ unittest.TestCase created to run unittests on helicopter.azimuthshandler.AzimuthsHandler
    class
    """
    @patch('logging.getLogger')
    def setUp(self, mock_logger):
        """ Set-up method for unittests. Creates an AzimuthsHandler object instance. Creates a mock
        config file that predefines azimuth data
        """
        mesh_id = 0
        data_id = 0
        precice_vertex_ids = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        dimensions = 3
        config = Mock(spec='utility.Config')
        config_attr = {
            "azimuths": [1, 2, 3, 4, 5]
        }
        config.configure_mock(**config_attr)
        args = [config]
        self.data_handler = AzimuthsHandler(mesh_id, data_id, precice_vertex_ids, dimensions,
                                            None, args)

    def test_read(self):
        """ Test read method in AzimuthsHandler. This method should throw a NotImplementedError

        """
        data = None
        iteration = 0
        with self.assertRaises(NotImplementedError):
            self.data_handler.read(data, iteration)

    def test_write(self):
        """ Test write method in AzimuthsHandler. The packaged information should be as written
        in helicopter.azimuthshandler.AzimuthsHandler documentation. Provide expected output for the
        mocked utility.Config object.
        """
        output = self.data_handler.write(0)
        self.assertIsInstance(output, list)
        self.assertEqual(1, len(output))
        self.assertEqual(16, len(output[0]))
        np.testing.assert_array_equal(output[0], [5, 1, 2, 3, 4, 5] + 10 * [0])

    def test_is_scalar(self):
        """ Test is_scalar method. Should return True as this is data handler handles scalar data
        """
        self.assertTrue(self.data_handler.is_scalar())

    def test_dimensions(self):
        """ Tests get_dimensions method. Should return dimension number listed in mocked
        utility.Config object.
        """
        self.assertEqual(self.data_handler.get_dimensions(), 3)


if __name__ == "__main__":
    unittest.main()
