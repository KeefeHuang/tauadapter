"""
@package tauadapter
This module contains the unittests for the motions data handler for the HelicopterSolver
"""
import re
import os
import unittest
import filecmp
from mock import Mock, patch


import numpy as np
from helicopter.motionshandler import MotionsHandler


# pylint: disable=arguments-differ, unused-argument
class MotionsHandlerTests(unittest.TestCase):
    """ unittest.TestCase created to run unittests on helicopter.motionshandler.MotionsHandler
    class
    """

    @patch('logging.getLogger')
    def setUp(self, mock_logger):
        """ Set-up method for unittests. Creates an MotionsHandler object instance. Creates a mock
        config file that predefines azimuth data
        """
        mesh_id = 0
        data_id = 0
        precice_vertex_ids = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        dimensions = 3
        self.config = Mock(spec='utility.Config')
        config_attr = {
            "azimuths": [5, 1, 2, 3, 4, 5],
            'blades': 4
        }
        self.config.configure_mock(**config_attr)
        args = [self.config]

        parafile = Mock()
        parafile_attr = {
            'get_para_value.return_value': 'helicopter/tests/motion'
        }
        parafile.configure_mock(**parafile_attr)

        self.data_handler = MotionsHandler(mesh_id, data_id, precice_vertex_ids, dimensions,
                                           parafile, args)

    def test_initialize(self):
        """ Test MotionsHandler constructor. Check if setting of motion file is correct
        """
        self.assertEqual(self.data_handler.motion_file, "helicopter/tests/motion")

    @patch('helicopter.motionshandler.MotionsHandler.write_motion_file')
    @patch('helicopter.motionshandler.MotionsHandler.get_frequency')
    def test_read(self, mock_get_frequency, mock_write_motion_file):
        """ Test read method in MotionsHandler. Check to see if data is extracted from passed data
            properly. Check if output coefficients are correctly determined.
        """
        iteration = 0
        data = np.array([0.5, 3,
                         0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3,
                         0.2, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3,
                         0.3, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3,
                         0.4, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3,
                         0.5, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3,
                         0.6, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3,
                        ])
        self.data_handler.read(data, iteration)
        coefficients = self.config.coefficients
        np.testing.assert_array_equal(data, coefficients)

    def test_get_frequency(self):
        """ Test get_frequency. Ensure that it reads motion file value and correctly sets the
        frequency value
        """
        parafile_attr = {
            'get_para_value.return_value': 32.7
        }
        parafile = self.data_handler.parafile
        parafile.configure_mock(**parafile_attr)
        self.data_handler.get_frequency(parafile, 0.5)
        self.assertAlmostEqual(self.data_handler.frequency, 0.0152905198)

    # def test_extract_motion_file(self):
    #     """ Test extract_motion_file. Checks how chunks of text are extracted from motion file.
    #     Provide sample motion file and check against method output
    #     """
    #     sine_pattern = re.compile(
    #         r"Hinge - Fourier coefficients for rotation \(sin\)\s*:([\s0-9\.-]+)\n")
    #     cosine_pattern = re.compile(
    #         r"Hinge - Fourier coefficients for rotation \(cos\)\s*:([\s0-9\.-]+)\n")
    #     frequency_pattern = re.compile(r"Hinge - reduced frequency for rotation\s*:([\s0-9\.]+)\n")
    #
    #     cosine_text, _, _, _ = \
    #         self.data_handler.extract_motion_file_text(
    #             cosine_pattern, frequency_pattern, sine_pattern)
    #
    #     sample_sine_text = '\n          Hinge - reduced frequency reference length: 1\n     Hinge' \
    #                        ' - Fourier coefficients for rotation (cos):'
    #     self.assertEqual(cosine_text[1], sample_sine_text)

    def test_write_motion_file(self):
        """ Test write_motion_file. Write motion file based on sample inputs. Check inputs against
        sample file 'sample_motion'
        """
        sample_flap = [0.2, 0.3, 0.4]
        sample_lag = [0.5, 0.6, 0.7]
        sample_pitch = [0.8, 0.9, 1.0]
        sample_coefficients = (sample_flap, sample_lag, sample_pitch)
        self.data_handler.frequency = 0.5
        self.data_handler.write_motion_file(sample_coefficients, 0, 3)

        self.assertTrue(filecmp.cmp("helicopter/tests/sample_motion", "helicopter/tests/motion0"))

        os.remove("helicopter/tests/motion0")

    def test_write(self):
        """ Test write method in MotionsHandler. This method should throw a NotImplementedError
        """
        iteration = 0
        with self.assertRaises(NotImplementedError):
            self.data_handler.write(iteration)

    def test_is_scalar(self):
        """ Test is_scalar method. Should return True as this is data handler handles scalar data
        """
        self.assertTrue(self.data_handler.is_scalar())

    def test_dimensions(self):
        """ Tests get_dimensions method. Should return dimension number listed in mocked
        utility.Config object.
        """
        self.assertEqual(self.data_handler.get_dimensions(), 3)


if __name__ == "__main__":
    unittest.main()
