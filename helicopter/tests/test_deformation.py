"""
This module contains the unittests for HelicopterSolver class
"""
import unittest
import math
import filecmp
from mock import patch, Mock
import numpy as np
from scipy.interpolate import splrep, splev
from scipy.sparse import block_diag
from utility import config, rotation_matrix


# pylint: disable=unused-argument, arguments-differ
class HelicopterSolverDeformationTest(unittest.TestCase):
    """ unittest for writing Helisolver deformation files """

    @patch('logging.getLogger')
    def setUp(self, mock_logger):
        """ Test constructor of HelicopterSolver class."""
        self.config_dict = config("helicopter/tests/test_config.yaml")

    # def test_write_deformation_file(self):
    #     """ Test write_deformation_file method. Checks that output of deformation file is correct
    #     """
    #     deltas = np.array([0.1, 0.2, 0.3, 0.4, 0.5, 0.6])
    #     file_name = "helicopter/tests/test.scat"
    #     sample_file_name = "helicopter/tests/sample.scat"
    #     intermediate_grid = np.array([1, 2, 3, 4, 5, 6])
    #     self.deformation.write_deformation_file(deltas, file_name, intermediate_grid)
    #     self.assertTrue(filecmp.cmp(file_name, sample_file_name))
    #
    # def test_write_deformation_file_diff_arrays(self):
    #     """ Test write_deformation_file method. Feed two different size arrays. Method should
    #     throw an AssertionError.
    #     """
    #     deltas = np.array([0.1, 0.2, 0.3])
    #     file_name = "test.scat"
    #     intermediate_grid = np.array([1, 2, 3, 4, 5, 6])
    #     with self.assertRaises(AssertionError):
    #         self.deformation.write_deformation_file(deltas, file_name, intermediate_grid)
    #
    # def test_write_deformation_file_incorrect_length(self):
    #     """ Test write_deformation_file method. Feed arrays with lenghts not divisble by 3.
    #     Method should throw an AssertionError.
    #     """
    #     deltas = np.array([0.1, 0.2, 0.3, 0.4])
    #     file_name = "test.scat"
    #     intermediate_grid = np.array([1, 2, 3, 4])
    #     with self.assertRaises(AssertionError):
    #         self.deformation.write_deformation_file(deltas, file_name, intermediate_grid)

    def test_create_fourier_series(self):
        """ Test create_fourier_series method. Provides a sample fourier series and checks with
        calculated solution"""
        num_coefficients = 3
        flap = [1, 2, 3]
        lag = [4, 5, 6]
        pitch = [7, 8, 9]
        coefficients = flap + lag + pitch
        # Note that fourier series coefficients are configured as follows:
        # B0 + B1*cos(wt) + B2*sin(wt) + B3*cos(2wt) + B4*sin(2wt) ...
        fourier_func = self.deformation.create_fourier_function(num_coefficients, coefficients)

        # At angle = 0, sin(kwt) = 0, cost(kwt) = 1
        # output is equal to the sum of the first two coefficients in a series of 3 coefficients
        position = fourier_func(0)
        np.testing.assert_array_almost_equal(position, [3, 9, 15])

        # At angle = math.pi/2, sin(kwt) = 1, cost(kwt) = 0
        # output is equal to the sum of the first and third coefficients in a series of 3
        # coefficients
        position = fourier_func(math.pi / 2)
        np.testing.assert_array_almost_equal(position, [4, 10, 16])

    def test_get_fourier_functions(self):
        """ Test get_fourier_functions. Provides a sample series of coefficients and checks each
        output fourier function"""

        self.config_dict.collocations = [3]
        self.config_dict.coefficients = \
            [0, 3,
             1, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2,
             2, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2,
             3, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, 3.1, 3.2]

        fourier_list = self.deformation.get_fourier_functions()
        angle = 0

        # At angle = 0, sin(kwt) = 0, cost(kwt) = 1
        # output is equal to the sum of the first two coefficients in a series of 3 coefficients
        np.testing.assert_array_almost_equal(fourier_list[0](angle), [0.3, 0.9, 1.5])
        np.testing.assert_array_almost_equal(fourier_list[1](angle), [2.3, 2.9, 3.5])
        np.testing.assert_array_almost_equal(fourier_list[2](angle), [4.3, 4.9, 5.5])

        # At angle = math.pi/2, sin(kwt) = 1, cost(kwt) = 0
        # output is equal to the sum of the first and third coefficients in a series of 3
        # coefficients
        angle = math.pi / 2
        np.testing.assert_array_almost_equal(fourier_list[0](angle), [0.4, 1.0, 1.6])
        np.testing.assert_array_almost_equal(fourier_list[1](angle), [2.4, 3.0, 3.6])
        np.testing.assert_array_almost_equal(fourier_list[2](angle), [4.4, 5.0, 5.6])

    # def test_create_intermediate_grid(self):
    #     """ Test for create_intermediate_grid method. Provides sample collocation and
    #     coefficients, checks intermediate grid with sample intermediate grid"""
    #     self.config_dict.collocations = [3, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    #     self.config_dict.num_widthwise_points = 4
    #     self.config_dict.num_lengthwise_points = 7
    #     self.config_dict.blade_back = -0.05
    #     self.config_dict.blade_front = 0.1
    #     num_collocation_points = 4
    #     intermediate_grid = self.deformation.create_intermediate_grid(num_collocation_points)
    #
    #     sample_intermediate_grid = np.array([
    #         [-0.05, 1.0, 0.0], [0.0, 1.0, 0.0], [0.05, 1.0, 0.0], [0.1, 1.0, 0.0],
    #         [-0.05, 2.5, 0.0], [0.0, 2.5, 0.0], [0.05, 2.5, 0.0], [0.1, 2.5, 0.0],
    #         [-0.05, 4.0, 0.0], [0.0, 4.0, 0.0], [0.05, 4.0, 0.0], [0.1, 4.0, 0.0],
    #         [-0.05, 5.5, 0.0], [0.0, 5.5, 0.0], [0.05, 5.5, 0.0], [0.1, 5.5, 0.0],
    #         [-0.05, 7.0, 0.0], [0.0, 7.0, 0.0], [0.05, 7.0, 0.0], [0.1, 7.0, 0.0],
    #         [-0.05, 8.5, 0.0], [0.0, 8.5, 0.0], [0.05, 8.5, 0.0], [0.1, 8.5, 0.0],
    #         [-0.05, 10., 0.0], [0.0, 10., 0.0], [0.05, 10., 0.0], [0.1, 10., 0.0],
    #     ]).flatten()
    #
    #     np.testing.assert_array_almost_equal(intermediate_grid, sample_intermediate_grid)

    # pylint: disable=invalid-name,too-many-locals
    # def test_calculate_delta_matrix(self):
    #     """ Test calculate_delta_matrix method. Provides sample fourier series functions"""
    #     self.config_dict.collocations = [3, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    #     self.config_dict.num_widthwise_points = 4
    #     self.config_dict.num_lengthwise_points = 7
    #     self.config_dict.blade_back = -0.05
    #     self.config_dict.blade_front = 0.1
    #     axis_coefficients = [1, 1, 1]
    #     fourier_function1 = Mock(return_value=[math.pi, math.pi + 1, math.pi + 2])
    #     fourier_function2 = Mock(return_value=[math.pi / 3, math.pi / 3 + 1, math.pi / 3 + 2])
    #     fourier_function3 = Mock(return_value=[math.pi / 2, math.pi / 2 + 1, math.pi / 2 + 2])
    #     fourier_function4 = Mock(return_value=[math.pi / 4, math.pi / 4 + 1, math.pi / 4 + 2])
    #     fourier_functions = [fourier_function1, fourier_function2, fourier_function3,
    #                          fourier_function4]
    #
    #     x = [1, 4, 7, 10]
    #     xnew = [1, 2.5, 4, 5.5, 7, 8.5, 10]
    #
    #     flap_angles = [math.pi, math.pi / 3, math.pi / 2, math.pi / 4]
    #     lag_angles = [math.pi + 1, math.pi / 3 + 1, math.pi / 2 + 1, math.pi / 4 + 1]
    #     pitch_angles = [math.pi + 2, math.pi / 3 + 2, math.pi / 2 + 2, math.pi / 4 + 2]
    #
    #     flap_spline_interpolation = splrep(x, flap_angles, s=0)
    #     lag_spline_interpolation = splrep(x, lag_angles, s=0)
    #     pitch_spline_interpolation = splrep(x, pitch_angles, s=0)
    #
    #     new_flap_angles = splev(xnew, flap_spline_interpolation, der=0)
    #     new_lag_angles = splev(xnew, lag_spline_interpolation, der=0)
    #     new_pitch_angles = splev(xnew, pitch_spline_interpolation, der=0)
    #
    #     num_collocation_points = 4
    #
    #     angle = math.radians(45)  # pylint: disable=assignment-from-no-return
    #
    #     delta_matrix = \
    #         self.deformation.calculate_delta_matrix(angle, fourier_functions,
    #                                                 num_collocation_points)
    #
    #     sample_intermediate_grid = np.array([
    #         [-0.05, 1.0, 0.0], [0.0, 1.0, 0.0], [0.05, 1.0, 0.0], [0.1, 1.0, 0.0],
    #         [-0.05, 2.5, 0.0], [0.0, 2.5, 0.0], [0.05, 2.5, 0.0], [0.1, 2.5, 0.0],
    #         [-0.05, 4.0, 0.0], [0.0, 4.0, 0.0], [0.05, 4.0, 0.0], [0.1, 4.0, 0.0],
    #         [-0.05, 5.5, 0.0], [0.0, 5.5, 0.0], [0.05, 5.5, 0.0], [0.1, 5.5, 0.0],
    #         [-0.05, 7.0, 0.0], [0.0, 7.0, 0.0], [0.05, 7.0, 0.0], [0.1, 7.0, 0.0],
    #         [-0.05, 8.5, 0.0], [0.0, 8.5, 0.0], [0.05, 8.5, 0.0], [0.1, 8.5, 0.0],
    #         [-0.05, 10., 0.0], [0.0, 10., 0.0], [0.05, 10., 0.0], [0.1, 10., 0.0],
    #     ])
    #     sample_intermediate_grid = np.concatenate(sample_intermediate_grid)
    #     deltas = delta_matrix.dot(sample_intermediate_grid)
    #
    #     sample_motion_matrix = rotation_matrix(new_pitch_angles[-1], 2, dim='deg').dot(
    #         rotation_matrix(new_flap_angles[-1], 1, dim='deg')).dot(
    #             rotation_matrix(new_lag_angles[-1], 3, dim='deg'))
    #
    #     sample_deformation_matrix_1 = rotation_matrix(new_pitch_angles[0], 2, dim='deg').dot(
    #         rotation_matrix(new_flap_angles[0], 1, dim='deg')).dot(
    #             rotation_matrix(new_lag_angles[0], 3, dim='deg'))
    #
    #     sample_deformation_matrix_1_5 = rotation_matrix(new_pitch_angles[1], 2, dim='deg').dot(
    #         rotation_matrix(new_flap_angles[1], 1, dim='deg')).dot(
    #             rotation_matrix(new_lag_angles[1], 3, dim='deg'))
    #
    #     sample_deformation_matrix_2 = rotation_matrix(new_pitch_angles[2], 2, dim='deg').dot(
    #         rotation_matrix(new_flap_angles[2], 1, dim='deg')).dot(
    #             rotation_matrix(new_lag_angles[2], 3, dim='deg'))
    #
    #     sample_deformation_matrix_2_5 = rotation_matrix(new_pitch_angles[3], 2, dim='deg').dot(
    #         rotation_matrix(new_flap_angles[3], 1, dim='deg')).dot(
    #             rotation_matrix(new_lag_angles[3], 3, dim='deg'))
    #
    #     sample_deformation_matrix_3 = rotation_matrix(new_pitch_angles[4], 2, dim='deg').dot(
    #         rotation_matrix(new_flap_angles[4], 1, dim='deg')).dot(
    #             rotation_matrix(new_lag_angles[4], 3, dim='deg'))
    #
    #     sample_deformation_matrix_3_5 = rotation_matrix(new_pitch_angles[5], 2, dim='deg').dot(
    #         rotation_matrix(new_flap_angles[5], 1, dim='deg')).dot(
    #             rotation_matrix(new_lag_angles[5], 3, dim='deg'))
    #
    #     sample_deformation_matrix_4 = rotation_matrix(new_pitch_angles[6], 2, dim='deg').dot(
    #         rotation_matrix(new_flap_angles[6], 1, dim='deg')).dot(
    #             rotation_matrix(new_lag_angles[6], 3, dim='deg'))
    #
    #     M = block_diag([sample_motion_matrix] * 28)
    #     D = block_diag([sample_deformation_matrix_1] * 4 + [sample_deformation_matrix_1_5] * 4 +
    #                    [sample_deformation_matrix_2] * 4 + [sample_deformation_matrix_2_5] * 4 +
    #                    [sample_deformation_matrix_3] * 4 + [sample_deformation_matrix_3_5] * 4 +
    #                    [sample_deformation_matrix_4] * 4)
    #     sample_q = M.T.dot(D.dot(sample_intermediate_grid) - M.dot(
    #         sample_intermediate_grid)).reshape((1, -1))
    #     sample_q_2 = (M.T.dot(D) - np.eye(84)).dot(sample_intermediate_grid)
    #
    #     np.testing.assert_array_almost_equal(sample_q, sample_q_2)
    #
    #     np.testing.assert_array_almost_equal(deltas.reshape((1, -1)), sample_q)


if __name__ == "__main__":
    unittest.main()
