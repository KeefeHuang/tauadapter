"""
@package tauadapter
This module contains the unittests for the forces/moments data handler for the HelicopterSolver
"""
# pylint: disable=wrong-import-position
import unittest
import math
import sys
from mock import Mock, MagicMock, patch

sys.modules['tau_python'] = MagicMock()
sys.modules['precice'] = MagicMock()

import numpy as np
from helicopter.forcesmomentshandler import ForcesMomentsHandler
from utility import rotation_matrix


# pylint: disable=arguments-differ, unused-argument
class ForcesMomentsHandlerTests(unittest.TestCase):
    """ unittest.TestCase created to run unittests on
    helicopter.forcesmomentshandler.ForcesMomentsHandler class
    """

    @patch('tau_python.tau_mpi_nranks', return_value=0)
    @patch('logging.getLogger')
    def setUp(self, mock_logger, mock_mpi_rank):
        """ Set-up method for unittests. Creates an AzimuthsHandler object instance. Creates a mock
        config file that predefines azimuth data
        """
        mesh_id = 0
        data_id = 0
        precice_vertex_ids = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
        tau_vertex_ids = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
        coordinates = np.array([
            [0, 0.05, 0],
            [0, 0.05, 1],
            [0, 0.15, 0],
            [0, 0.25, 1],
            [1, 0.35, 0],
            [1, 0.45, 1],
            [1, 0.55, 0],
            [1, 0.55, 1],
            [0, 0.65, 0],
            [0, 0.67, 1],
            [0, 0.7, 0],
            [0, 0.8, 1],
            [1, 0.9, 0],
            [1, 1.0, 1],
            [1, 1.1, 0],
            [1, 1.1, 1]
        ])
        dimensions = 3
        self.interface = Mock(spec='interface.Interface')
        interface_attr = {
            "write_data": [['test_17']],
            "blade": -1
        }
        self.interface.configure_mock(**interface_attr)

        self.config = Mock(spec='utility.Config')
        config_attr = {
            'azimuths': [5, 1, 2, 3, 4, 5],
            'total_azimuths': 40,
            'interpolation_type': 'piece-wise',
            'blades': 5,
            'output_location': './helicopter/tests/',
            'collocations': np.array([0, 0, 0, 0, 0, 1, 0, 1, 0]),
            'iterations_per_revolution': 360,
            'rotation_order': '213',
            'revolutions': [2],
            'initial_revolutions': 0
        }
        self.config.configure_mock(**config_attr)
        args = (tau_vertex_ids, coordinates, self.config, self.interface)
        self.data_handler = ForcesMomentsHandler(mesh_id, data_id, precice_vertex_ids, dimensions,
                                                 None, args)

    def tearDown(self):
        """ Tear-down method for unittests. Resets utility.Config Mock.
        """
        self.data_handler.config.reset_mock()

    def test_initialize_piecewise(self):
        """ Tests constructor method and set_iteration_modifier for piece-wise interpolation.
        Ensures that the various search patterns are correctly calculated for piece-wise
        interpolation.
        """
        pattern_prefix = self.data_handler.pattern_prefix
        pattern_suffix = self.data_handler.pattern_suffix
        iteration_modifier = self.data_handler.iteration_modifier
        self.assertEqual(r".*surface.*i=", pattern_prefix)
        self.assertEqual(r"_[^pl\n]*", pattern_suffix)
        self.assertEqual(-63, iteration_modifier)

    def test_initialize_full(self):
        """ Tests constructor method and set_iteration_modifier. Ensures that the iteration
        modifier is correctly calculated for full-interpolation
        """
        self.config.interpolation_type = 'full'
        self.data_handler.set_iteration_modifier(17)
        iteration_modifier = self.data_handler.iteration_modifier
        self.assertEqual(-207, iteration_modifier)

    def test_set_collocation_points(self):
        """ Test set_collocation_points method. This method takes in sample collocation point data
        and assigns all points in TAU grid to a specific collocation point. This used for
        force/moment calculation.
        """
        self.config.collocations = \
            np.array([6, 0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2,
                      0.0, 0.1, 0.0,
                      0.0, 0.3, 0.2,
                      0.0, 0.5, 0.2,
                      0.0, 0.7, 0.2,
                      0.0, 0.9, 0.2,
                      0.0, 1.1, 0.2])

        sample_r_edges = np.array([0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2])
        sample_collocation_points = np.array([[0., 0.1, 0.],
                                              [0., 0.3, 0.2],
                                              [0., 0.5, 0.2],
                                              [0., 0.7, 0.2],
                                              [0., 0.9, 0.2],
                                              [0., 1.1, 0.2]])
        sample_collocation_point_ids = np.array([0, 0, 0, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 5, 5, 5])
        self.data_handler.set_collocation_points()
        np.testing.assert_array_equal(sample_r_edges, self.data_handler.r_edges)
        np.testing.assert_array_equal(sample_collocation_points,
                                      self.data_handler.collocation_points)
        np.testing.assert_array_equal(sample_collocation_point_ids,
                                      self.data_handler.collocation_point_ids)

    def test_calculate_rotation_matrices(self):
        """ Test calculate_rotation_matrices method.
        """
        self.config.shaft_pitch = 1.3
        self.config.shaft_roll = 1.9
        self.config.coefficients = \
            [0.5, 3,
             0.1, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2,
             0.2, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2,
             0.3, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2,
             0.4, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2,
             0.5, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2,
             0.6, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3]
        self.config.iterations_per_revolution = 360
        current_iteration = 720
        blade_motion_rotation_matrix, shaft_rotation_matrix = \
            self.data_handler.calculate_rotation_matrices(current_iteration)
        flap_angle = 0.5
        lag_angle = 1.1
        pitch_angle = 1.7
        dcm1 = np.transpose(rotation_matrix(-flap_angle, 1, dim='deg'))
        dcm2 = np.transpose(rotation_matrix(pitch_angle, 2, dim='deg'))
        dcm3 = np.transpose(rotation_matrix(-lag_angle, 3, dim='deg'))
        sample_blade_motion_rotation_matrix = dcm2.dot(dcm1).dot(dcm3).T

        np.testing.assert_array_almost_equal(blade_motion_rotation_matrix,
                                             sample_blade_motion_rotation_matrix)
        blade_spacing_angle = 2 * math.pi / self.config.blades
        blade_angle = -63 + np.degrees(blade_spacing_angle) * (
            -1)
        positional_rotation_matrix = rotation_matrix(-blade_angle, 3, dim='deg')
        shaft_pitch_rotation_matrix = rotation_matrix(self.config.shaft_pitch, 2, dim='deg')
        shaft_roll_rotation_matrix = rotation_matrix(self.config.shaft_roll, 1, dim='deg')
        sample_positional_rotation_matrix = shaft_pitch_rotation_matrix.dot(
            shaft_roll_rotation_matrix).dot(positional_rotation_matrix)

        np.testing.assert_array_almost_equal(sample_positional_rotation_matrix,
                                             shaft_rotation_matrix)

    @patch('numpy.save')
    @patch('tau_python.tau_solver_this_timestep', return_value=2)
    @patch('tau_python.tau_mpi_rank', return_value=0)
    @patch('helicopter.forcesmomentshandler.read_grid',
           return_value={'global_id': np.array([4, 3, 1, 2, 5, 0]),
                         'geodesic_x-force': np.array([4., 3., 1., 2., 5., 0.0]),
                         'geodesic_y-force': np.array([8., 6., 2., 4., 10., 0.0]),
                         'geodesic_z-force': np.array([12., 9., 3., 6., 15., 0.0]),
                         'geodesic_x': np.array([0.4, 0.3, 0.1, 0.2, 0.5, 0.0]),
                         'geodesic_y': np.array([0.8, 0.6, 0.2, 0.4, 1.0, 0.0]),
                         'geodesic_z': np.array([1.2, 0.9, 0.3, 0.6, 1.5, 0.0])})
    def test_process_data(self, mock_read_grid, mock_this_timestep, mock_mpi_rank, mock_numpy):
        """ Test process_data method. Checks to see if data is correctly extract from provided file.
        read_grid function is patched to provide an easily checked data_dict. Sample forces and
        coordinate data is patched and sample solutions are provided to check against. tau_python
        methods are also patched to see if they are called correctly.
        """
        self.data_handler.length = 3
        self.data_handler.vertex_ids = np.array([1, 2, 3])

        force_buffer, coord_buffer = self.data_handler.process_data("Test Reading")
        mock_read_grid.assert_called_with("Test Reading")
        sample_force_buffer = np.array([
            [1., 2., 3.],
            [2., 4., 6.],
            [3., 6., 9.]])

        sample_coord_buffer = np.array([
            [0.1, 0.2, 0.3],
            [0.2, 0.4, 0.6],
            [0.3, 0.6, 0.9]])

        np.testing.assert_array_almost_equal(force_buffer, sample_force_buffer)
        np.testing.assert_array_almost_equal(coord_buffer, sample_coord_buffer)
        # mock_this_timestep.assert_called_with()
        # mock_mpi_rank.assert_called_with()
        #
        # mock_numpy.assert_any_call("force-61_0", force_buffer)
        # mock_numpy.assert_called_with("coord-61_0", coord_buffer)

    def test_read(self):
        """ Test read method in ForcesMomentsHandler. This method should throw a NotImplementedError
        """
        data = None
        iteration = 0
        with self.assertRaises(NotImplementedError):
            self.data_handler.read(data, iteration)

    # pylint: disable=unused-variable
    @patch('helicopter.forcesmomentshandler.ForcesMomentsHandler.calculate_rotation_matrices',
           return_value=(
               np.array([[1, 0, 0], [0, np.cos(math.pi / 3), -np.sin(math.pi / 3)],
                         [0, np.sin(math.pi / 3), np.cos(math.pi / 3)]]),
               np.array([[np.cos(math.pi / 2), -np.sin(math.pi / 2), 0],
                         [np.sin(math.pi / 2), np.cos(math.pi / 2), 0], [0, 0, 1]])))
    @patch("tau_python.tau_solver_this_timestep", return_value=720)
    @patch("helicopter.forcesmomentshandler.ForcesMomentsHandler.process_data",
           return_value=(
               np.array([[1.0, 1.0, 1.0], [2.0, 2.0, 2.0], [3.0, 3.0, 3.0]]),
               np.array([[0.01, 0.1, 0.05], [0.02, 0.2, 0.10], [0.03, 0.3, 0.15]])))
    @patch("helicopter.forcesmomentshandler.ForcesMomentsHandler.set_collocation_points")
    @patch("logging.getLogger")
    def test_write(self, mock_logger, mock_set_collocation, mock_process_data, mock_tau_timestep,
                   mock_rotation_matrix):
        """ Test write method in ForcesMomentsHandler. This method should read a TAU surface
        output file and write the force and moment data to preCICE. This test patches the logger,
        the set_collocation method, process_data method, calculate_rotation_matrices method to
        output sample data. These methods are all tested individually.

        A sample case is presented with a specified force/moment data and rotation matrices
        """

        # Set iteration modifier
        self.data_handler.iteration_modifier = -10
        # Set constants used to find TAU output files
        self.data_handler.output_path = "./helicopter/tests/"
        self.data_handler.pattern_prefix = "prefix"
        self.data_handler.pattern_suffix = "suffix"

        # motion = np.array([[1, 0, 0], [0, np.cos(math.pi / 3), -np.sin(math.pi / 3)],
        #                    [0, np.sin(math.pi / 3), np.cos(math.pi / 3)]])

        # Set data_handler collocation point array
        collocation_points = self.data_handler.collocation_points = np.array(
            [[0.0, 0.1, 0.0], [0.2, 0.2, 1.0], [0.3, 0.3, 1.5]])

        # base_points = np.array([[0.01, 0.1, 0.05], [0.02, 0.2, 0.10], [0.03, 0.3, 0.15]])
        sample_force = np.array([[-1.0, -1.0, -1.0], [-2.0, -2.0, -2.0], [-3.0, -3.0, -3.0]])
        sample_moment = np.array([0.096603, -0.136603, 0.04, -0.345641, -1.14641, 1.492051,
                                   -0.777691, -2.579423, 3.357114])
        # base_moment_arm = base_points - collocation_points
        # sample_moment = np.cross(sample_force, base_moment_arm)

        # sample_force = sample_force.dot(motion.T)
        # sample_moment = sample_moment.dot(motion.T)
        self.data_handler.collocation_point_ids = np.array([0, 1, 2])

        data = self.data_handler.write(1)

        # mock_tau_timestep.assert_called_with()
        mock_set_collocation.assert_called_with()
        # mock_rotation_matrix.assert_called_with(710)
        mock_process_data.assert_called_with("./helicopter/tests/prefix1430suffix")

        force_data = data[0]
        np.testing.assert_array_almost_equal(force_data, sample_force.flatten())
        moment_data = data[1]
        np.testing.assert_array_almost_equal(moment_data, sample_moment.flatten())

    def test_is_scalar(self):
        """ Test is_scalar method. Should return False as this is data handler handles scalar data
        """
        self.assertFalse(self.data_handler.is_scalar())

    def test_dimensions(self):
        """ Tests get_dimensions method. Should return dimension number listed in mocked
        utility.Config object.
        """
        self.assertEqual(self.data_handler.get_dimensions(), 3)


if __name__ == "__main__":
    unittest.main()
