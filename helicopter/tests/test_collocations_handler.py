"""
@package tauadapter
This module contains the unittests for the collocations data handler for the HelicopterSolver
"""
import unittest
from mock import Mock, patch

import numpy as np
from helicopter.collocationshandler import CollocationsHandler


# pylint: disable=arguments-differ, unused-argument
class CollocationsHandlerTests(unittest.TestCase):
    """ unittest.TestCase created to run unittests on
    helicopter.collocationshandler.CollocationsHandler class
    """

    @patch('logging.getLogger')
    def setUp(self, mock_logger):
        """ Set-up method for unittests. Creates an AzimuthsHandler object instance. Creates a mock
        config file that predefines azimuth data
        """
        mesh_id = 0
        data_id = 0
        precice_vertex_ids = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        dimensions = 3
        self.config = Mock(spec='utility.Config')
        config_attr = {
            "azimuths": [5, 1, 2, 3, 4, 5]
        }
        self.config.configure_mock(**config_attr)
        args = [self.config]
        self.data_handler = CollocationsHandler(mesh_id, data_id, precice_vertex_ids, dimensions,
                                                None, args)

    def tearDown(self):
        """ Tear-down method for unittests. Resets utility.Config mock.
        """
        self.config.reset_mock()

    def test_read(self):
        """ Test read method in AzimuthsHandler. This method should store the read data in the
        utility.Config object
        """
        data = np.array([5, 4, 3, 2, 1])
        iteration = 0
        self.data_handler.read(data, iteration)
        self.assertIsInstance(self.config.collocations, list)
        np.testing.assert_array_equal(data, self.config.collocations)

    def test_write(self):
        """ Test read method in AzimuthsHandler. This method should throw a NotImplementedError
        """
        iteration = 0
        with self.assertRaises(NotImplementedError):
            self.data_handler.write(iteration)

    def test_is_scalar(self):
        """ Test is_scalar method. Should return True as this is data handler handles scalar data
        """
        self.assertTrue(self.data_handler.is_scalar())

    def test_dimensions(self):
        """ Tests get_dimensions method. Should return dimension number listed in mocked
        utility.Config object.
        """
        self.assertEqual(self.data_handler.get_dimensions(), 3)


if __name__ == "__main__":
    unittest.main()
