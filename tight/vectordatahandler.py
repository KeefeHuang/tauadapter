"""
@package tauadapter
This module contains the VectorHandler class. This class is a general subclass of the
datahandler.DataHandler class and handles vector data from TAU to preCICE.
"""

import logging
import numpy as np
import tau_python  # pylint: disable=import-error
from tauadapter.datahandler import DataHandler
from tauadapter.tauinterface import TauInterface
import tauadapter.constants as constant


class VectorHandler(DataHandler):
    """ VectorHandler class. Handles force data from TAU and writes data to preCICE

    Attributes:
    """

    def __init__(self, args):
        """ Constructor for ForcesHandler class.
        Arguments:
            args (dict): dict of input variables storing datahandler information
        """
        # Call DataHandler base class constructor
        super(VectorHandler, self).__init__(args)

        self.tau_interface = TauInterface()
        rank = tau_python.tau_mpi_rank()
        self.logger = logging.getLogger("tau" + str(rank) + ".VectorHandler")
        self.data_types = args[constant.DATA_TYPE]

    # Looks for data in output files matching pattern, parses data and passes to interface
    def write(self, _iteration):
        """ Writes force data to preCICE

        Arguments:
            _iteration (int): current coupled iteration as int

        Returns:
            data (ndarray): 1D array of vector data arranged as [fx1, fy1, fz1, fx2, fy2, fz2, ...]
        """
        tau_python.tau_solver_buffer_surface()
        surface_stream = tau_python.tau_get_surface_stream_name()
        global_ids = self.tau_interface.get_int_variable(surface_stream, 'global_id')

        _, indexing, _ = \
            np.intersect1d(global_ids, self.vertex_ids, return_indices=True)
        x_data = self.tau_interface.get_float_variable(surface_stream, self.data_types[0])[indexing]
        y_data = self.tau_interface.get_float_variable(surface_stream, self.data_types[1])[indexing]
        z_data = self.tau_interface.get_float_variable(surface_stream, self.data_types[2])[indexing]

        data = np.array(zip(x_data, y_data, z_data)).flatten()

        return data

    def read(self, data, iteration):
        """ Reads force data from preCICE. This is not implemented for force data for the FlapSolver
         case.

        Arguments:
            data (ndarray): data received from preCICE. This is in a 1D array of size (n x 3)
            iteration (int): current coupled iteration as int
        """

        self.logger.error("Read is not implemented for Forces for this module\n")

    def is_scalar(self):
        """
        Returns:
             Returns false as this is a vector data type
        """
        return False
