"""
@package tauadapter
This module contains the DisplacementsHandler class. This class is a subclass of the
datahandler.DataHandler class used for the TightSolver case and handles displacement data passed
from preCICE and stores processed data in the config.Config object shared by all DataHandlers.
"""
import logging
import numpy as np
from mpi4py import MPI
from tauadapter.datahandler import DataHandler


class DisplacementsHandler(DataHandler):
    """ DisplacementsHandler class. Handles displacement data from preCICE and writes deformation
    scatfiles.

    Attributes:
        total_length (int): number of nodes on interface (n) for all processes. equal to self.length
            if number of processes is 1
    """

    def __init__(self, args):
        """ Constructor for DisplacementsHandler class.

        Arguments:
            args (dict): dictionary of datahandler arguments to be parsed by base class
        """
        super(DisplacementsHandler, self).__init__(args)

        self.simulation.file_name = None
        self.deform_path = self.tau_para.get_block_para_value(
            "RBF basis coordinates and deflections filename", 0, "group end")

        self.logger = logging.getLogger("tau" + str(MPI.COMM_WORLD.Get_rank()) +
                                        ".DisplacementHandler")
        self.rank = self.comm.Get_rank()
        # Number of indices modified
        if self.comm.Get_size() > 1:
            self.total_length = self.comm.allreduce(self.length, op=MPI.SUM)

    def write(self, _iteration):
        """ Implements datahandlers.DataHandler.write() abstract function. Write is not implemented
        for displacements in TightSolver

        Arguments:
            _iteration (int): current coupled iteration as int
        """
        self.logger.error("Write is not implemented for Displacements for this module\n")
        raise NotImplementedError

    def read(self, data, iteration):
        """ Implements datahandlers.DataHandler.read() abstract function. Receives data from the
        preCICE and writes to TAU scatfile

        Arguments:
            data (ndarray): data received from preCICE. This is in a 1D array of size (n x 3)
            displacement data is arranged as [dx1, dy1, dz1, dx2, dy2, dz2 ...]
            iteration (int): current coupled iteration as int
        """
        # Call to rearrange data in TAU
        # Data is now in tau format of XZY
        data = np.array(data)

        # Call parallel writing of deformation if in parallel
        self.simulation.file_name = self.deform_path.rsplit(".", 1)[0] + str(iteration) + "." + \
                                    self.deform_path.rsplit(".", 1)[1]

        if self.comm.Get_size() > 1:
            self.write_parallel_deformation_file(data)
        # else call serial writing of deformation file
        else:
            self.write_deformation_file(data)

    def is_scalar(self):
        """
        Returns:
             Returns false as this is a vector data type
        """
        return False

    def write_deformation_file(self, displacement_data):
        """ Function writes to deformation scatfile in serial

        Arguments:
            displacement_data (ndarray): data received from preCICE. This is in a 1D array of size
            (n x 3). displacement data is arranged as [dx1, dy1, dz1, dx2, dy2, dz2 ...]
        """

        with open(self.simulation.file_name, "w") as deformation_file:
            deformation_file.write(" " + str(self.length) + "\r\n")
            for i in range(self.length):
                deformation_file.write(
                    " {:3.5f} {:3.5f} {:3.5f} {:3.5f} {:3.5f} {:3.5f}\r\n".format(
                        self.coordinates[i * 3],
                        self.coordinates[i * 3 + 1],
                        self.coordinates[i * 3 + 2],
                        displacement_data[i * 3],
                        0,
                        displacement_data[i * 3 + 2]))

    # Function writes to deformation scatfile in parallel
    def write_parallel_deformation_file(self, displacement_data):
        """ Function writes to deformation scatfile in parallel. An aggregation step is done
        to gather all data to a single process before writing to file. Possible improvements to
        perform parallel IO.

        Arguments:
            displacement_data (ndarray): data received from preCICE. This is in a 1D array of size
            (n x 3). displacement data is arranged as [dx1, dy1, dz1, dx2, dy2, dz2 ...]
        """

        text = ""
        for i in range(self.length):
            text = text + " {:3.5f} {:3.5f} {:3.5f} {:3.5f} {:3.5f} {:3.5f}\r\n".format(
                self.coordinates[i * 3],
                self.coordinates[i * 3 + 1],
                self.coordinates[i * 3 + 2],
                displacement_data[i * 3],
                0,
                displacement_data[i * 3 + 2])
        total_text = self.comm.gather(text, root=0)

        if self.rank == 0:
            with open(self.simulation.file_name, "w") as deformation_file:
                deformation_file.write(" " + str(self.total_length) + "\r\n")
                deformation_file.write("".join(total_text))

    @staticmethod
    def set_scatfile_name(scatfile_name):
        """ Returns a dict used to update the 'RBF basis coordinates and deflections filename' in
        TAU parafile

        Arguments:
            scatfile_name: new deformation scatfile as str

        Returns:
            para_list: dictionary containing new deformation gridfile
        """
        para_list = {'RBF basis coordinates and deflections filename': scatfile_name}
        return para_list
