"""
@package tauadapter
@package tauadapter
This module contains the ForcesHandler class. This class is a subclass of the
datahandler.DataHandler class used for the TightSolver case and handles force data from TAU to
preCICE.
"""

import logging
import glob
from os.path import join
import numpy as np
import tau_python  # pylint: disable=import-error
from tauadapter.utility import read_grid
from tauadapter.datahandler import DataHandler


class ForcesHandler(DataHandler):
    """ ForcesHandler class. Handles force data from TAU and writes data to preCICE

    Attributes:
        pattern_prefix (str): output file prefix pattern, used to locate TAU output files for
            processing
        pattern_suffix (str): output file suffix pattern, used to locate TAU output files for
            processing
    """

    def __init__(self, args):
        """ Constructor for ForcesHandler class.
        Arguments:
            args (dict): dictionary of datahandler arguments to be parsed by base class
        """
        # Call DataHandler base class constructor
        super(ForcesHandler, self).__init__(args)
        # Prefix to search for correct output file
        self.pattern_prefix = "*surface*i="

        rank = tau_python.tau_mpi_rank()

        self.logger = logging.getLogger("tau" + str(rank) + ".ForcesHandler")

        # Adds domain flag suffix in case of parallel case
        if tau_python.tau_mpi_nranks() > 1:
            self.pattern_suffix = "_*domain_" + str(rank)
        else:
            self.pattern_suffix = "_*"

    # Looks for data in output files matching pattern, parses data and passes to interface
    def write(self, iteration):
        """ Implements datahandlers.DataHandler.write() abstract function. Writes force data to
            preCICE

        Arguments:
            iteration (int): current coupled iteration as int

        Returns:
            data (ndarray): 1D array of force data of size (self.length). pass force data arranged
            as [fx1, fy1, fz1, fx2, fy2, fz2, ...]
        """

        # Creates file pattern to search for output file
        output_file_pattern = self.pattern_prefix + str(iteration) + self.pattern_suffix
        output_files = [x for x in glob.glob(join(self.config.output_location,
                                                  output_file_pattern)) if
                        ".plt" not in x]
        # Extract data from output files
        print(join(self.config.output_location, output_file_pattern))
        
        data = self.process_data(output_files[0])

        return data

    def read(self, _data, _iteration):
        """ Implements datahandlers.DataHandler.read() abstract function. Read is not implemented
        for force data in TightSolver

        Arguments:
            _data (ndarray): data received from preCICE. This is in a 1D array of size (n x 3)
            _iteration (int): current coupled iteration as int
        """

        self.logger.error("Read is not implemented for Forces for this module\n")
        raise NotImplementedError

    def is_scalar(self):
        """
        Returns:
             Returns false as this is a vector data type
        """
        return False

    # Get data from output file
    def process_data(self, output_file):
        """ Function reads the name of a TAU output file. Extracts all force data, filters for
        vertices in this partition and arranges it to match the order of self.precice_vertex_ids

        Arguments:
            output_file (str): name of TAU output file as a str

        Return:
            data (ndarray): 1D array of force data arranged as [x0, y0, z0, x1, y1, z1, x2, y2, z2
            ...]
        """
        # Read netcdf file
        data_dict = read_grid(output_file)
        # Pull global id from netcdf file
        data_nodes = data_dict['global_id']
        # Determine which nodes belong to this MPI rank
        _, comm1, _ = np.intersect1d(data_nodes, self.vertex_ids,
                                     return_indices=True)
        # comm1 is the indices to reorder data such that it follows the order after intersect
        # function. In this case force data is sorted to match precice_vertex_id order.
        force_x_data = data_dict['x-force'][comm1]
        force_y_data = data_dict['y-force'][comm1]
        force_z_data = data_dict['z-force'][comm1]

        # Arrange data in format [x0, y0, z0, x1, y1, z1, x2, y2, z2 ...]
        data = np.array(zip(force_x_data, force_y_data, force_z_data)).flatten()
        return data
