"""
@package tauadapter
This module contains the TightSolver class that controls the execution loop of TAU when running a
 tightly-coupled simulation. This class performs the following actions in the given order when
 created and run:
1) Sets up the TAU solver based on the input parafile and sets up the datahandler.DataHandler
objects in the interface.DataInterface class.
2) Performs the execution loop (with or without preCICE) based on the given parafile
3) Calls interface.DataInterface class when data needs to be read or written from preCICE
"""


import re
import precice  # pylint: disable=import-error
import tau_python  # pylint: disable=import-error
from tau_python import *  # pylint: disable=import-error, wildcard-import

from tauadapter.solver import Solver
from tauadapter.utility import write_grid_deformation


class TightSolver(Solver):
    """ TightSolver class is the subclass of solver.Solver used for tightly-coupled simulations

    Attributes:
        data_interface (datainterface.DataInterface): interface.DataInterface object used to
            control data to and from preCICE
        rank (int): mpi rank of process as int
    """

    def __init__(self, config_file, data_interface):
        """ Constructor for subclass FlapSolver.
        Calls the constructor for the base class solver.Solver.
        Sets up logger and mpi rank

        Arguments:
            config_file (str): name of config file as str
            data_interface (datainterface.DataInterface): DataInterface object used to
            communicate with
                preCICE
        """
        super(TightSolver, self).__init__(config_file, data_interface)
        self.rank = tau_python.tau_mpi_rank()
        self.simulation = self.config.simulation
        self.simulation.file_name = None
        self.create_folder_structure()
        self.deformation_function = None
        self.logger.info("Initializing tau solver")
        self.tau_solver.init()
        self.deform_path = self.tau_para.get_block_para_value(
            "RBF basis coordinates and deflections filename", 0, "group end")

    def create_folder_structure(self):
        """ Setup folder structure based on input config file"""
        # Get original motion file from parafile. Prevents infinite appending of iteration number
        with open(str(self.config.parafile), "r") as parafile:
            parafile_text = parafile.read()
            output_pattern = re.compile(r"Output files prefix:\s*(.*)\n")
            output_prefix = re.search(output_pattern, parafile_text).group(1)

        self.config.output_location = output_prefix.rsplit("/", 1)[0]
        self.config.output_prefix = output_prefix.rsplit("/", 1)[1]

        # Create required output folders if missing
        if self.rank == 0:
            # Setup necessary and optional folders if possible.
            necessary_folders = ['output_location']
            optional_folders = ['restart_location', 'deform_location', 'scatfile_location']

            for folder in necessary_folders:
                self.create_necessary_folder(folder)

            for folder in optional_folders:
                self.create_optional_folder(folder)

    def tau_simulation_loop(self):
        """ Method that runs tightly-coupled TAU simulation

        Returns:
            timestep of solver as int
        """
        # Get values from solver to control simulation
        time_step = float(self.tau_para.get_para_value("Unsteady physical time step size"))
        if self.precice_interface.is_read_data_available():
            self.logger.info("Deforming grid with %s", self.simulation.file_name)
            self.tau_para.update(self.set_scatfile_name(self.simulation.file_name),
                                 block_key='group end', block_id=0,
                                 para_replace=1)
            primary_gridfile = self.tau_para.get_para_value('Primary grid filename')
            self.tau_para.update(self.set_para_primary_grid_name(primary_gridfile),
                                 para_replace=0)
            deformed_grid_name = './grid_deform/deform_{}.grid'.format(self.simulation.iteration)
            self.tau_para.update(self.set_para_deformed_mesh_name(deformed_grid_name),
                                 para_replace=0)
            self.tau_deform.run(read_primgrid=1, write_primgrid=1)

        self.tau_prep.run(write_dualgrid=0)
        self.tau_solver.init()
        self.logger.info("Starting outer loop for full multigrid")
        self.tau_solver.outer_loop()

        # Initialize outer loop with full multigrid start
        # tau_python.tau_solver_outer_loop_init()
        # Advance time and motion
        self.logger.info(
            'Iteration %d: Advancing time and motion for unsteady simulation',
            self.simulation.iteration)
        # tau_python.tau_solver_unsteady_advance_time()
        # tau_python.tau_solver_unsteady_advance_motion()
        # Start full multigrid
        # self.logger.info('Starting full multigrid')
        # tau_python.tau_solver_full_multigrid()
        # Run inner loop for number of iterations specified in parameter file
        # self.logger.info('Solving inner loop')
        # tau_python.tau_solver_inner_loop(0, tau_python.tau_solver_inner_loop_iteration_number(),
                                         # tau_python.tau_solver_min_residual())
        # Print monitoring values and increment physical time step
        tau_python.tau_solver_unsteady_monitoring()
        # Write results to files
        self.logger.info('Outputting results')
        # Write field data every fieldPeriod time steps if requested or if
        # it is the last iteration (to generate restart data)
        tau_python.tau_solver_write_output_conditional()
        # Finalize monitor history file (removes .tmp from file name)
        # tau_solver_output_monitor()
        if tau_python.tau_mpi_rank() == 0:
            self.tau2plt(str(self.config.parafile))
        self.tau_solver.stop()
        tau_python.tau_free_dualgrid()
        tau_python.tau_parallel_sync()
        return time_step

    def execute_precice(self):
        """ Implements abstract method solver.Solver.execute_precice(). Controls execution loop of
        main TAU simulation with preCICE.
        """
        self.tau_solver.stop()
        tau_python.tau_free_dualgrid()

        self.logger.info('Initialize preCICE interface')
        # self.create_deformation_file()
        tau_python.tau_parallel_sync()

        # Initialize preCICE interface and tau solver
        precice_tau = self.precice_interface.initialize()

        if self.precice_interface.is_action_required(precice.action_write_initial_data()):
            self.logger.warning("Checkpointing is not implemented in this solver. Please "
                                "implement checkpointing prior to using an Implicit coupling "
                                "scheme.")
            self.precice_interface.fulfilled_action(precice.action_write_initial_data())

        self.precice_interface.initialize_data()

        # Run main solver loop
        while self.precice_interface.is_coupling_ongoing():

            if self.precice_interface.is_action_required(
                    precice.action_write_iteration_checkpoint()):
                self.logger.warning("Checkpointing is not implemented in this solver. Please "
                                    "implement checkpointing prior to using an Implicit coupling "
                                    "scheme.")

                self.precice_interface.fulfilled_action(precice.action_write_iteration_checkpoint())

            time_step = self.tau_simulation_loop()

            # Perform data exchange
            self.data_interface.write_all(self.simulation.iteration)

            assert time_step <= precice_tau, \
                "Time step should always be smaller than preCICE timestep"
            self.precice_interface.advance(time_step)

            self.data_interface.read_all(self.simulation.iteration)
            if self.precice_interface.is_read_data_available():
                self.simulation.file_name = self.deform_path.rsplit(".", 1)[0] + str(
                    self.simulation.iteration) + "." + \
                                            self.deform_path.rsplit(".", 1)[1]

            if self.precice_interface.is_action_required(
                    precice.action_read_iteration_checkpoint()):  # i.e. not yet converged
                self.logger.warning("Checkpointing is not implemented in this solver. Please "
                                    "implement checkpointing prior to using an Implicit "
                                    "coupling scheme.")
                self.precice_interface.fulfilled_action(precice.action_read_iteration_checkpoint())

            self.simulation.iteration += 1

            tau_python.tau_parallel_sync()

        tau_python.tau_solver_output_field()
        tau_python.tau_solver_output_surface()
        tau_python.tau_parallel_sync()
        tau_python.tau_free_primgrid()
        tau_python.tau_free_prims()
        self.tau_solver.finalize()
        self.precice_interface.finalize()

    # # Run without preCICE
    def execute(self):
        """ Implements abstract method solver.Solver.execute(). Controls execution loop of main
        TAU simulation without preCICE.
        """
        deformation_defined = False
        if self.deformation_function is not None:
            self.logger.info("Deformation function defined.")
            deformation_defined = True
        else:
            self.logger.info("No deformation function defined. No deformation will occur")

        self.logger.info('Initialize solver')
        tau_python.tau_parallel_sync()
        # Initialize preCICE interface and tau solver
        self.logger.info("Initializing tau solver")
        self.tau_solver.init()

        # Get values from solver to control simulation
        min_residual = tau_python.tau_solver_min_residual()
        inner_n = tau_python.tau_solver_inner_loop_iteration_number()
        unsteady_time_steps = tau_python.tau_solver_outer_loop_iteration_number()
        time_step = float(self.tau_para.get_para_value("Unsteady physical time step size"))
        output_period = int(self.tau_para.get_para_value("Surface output period"))
        if self.config.iteration:
            pass
        else:
            self.config.iteration = 0

        primary_gridfile = self.tau_para.get_para_value('Primary grid filename')

        for _ in range(unsteady_time_steps + 1):

            self.logger.info("Starting outer loop for full multigrid")
            # Initialize outer loop with full multigrid start

            tau_python.tau_solver_outer_loop_init()

            # Advance time and motion
            self.logger.info('Iteration %d: Advancing time and motion for unsteady simulation',
                             self.config.iteration)
            tau_python.tau_solver_unsteady_advance_time()
            tau_python.tau_solver_unsteady_advance_motion()

            # Start full multigrid
            self.logger.info('Starting full multigrid')
            tau_python.tau_solver_full_multigrid()

            # Run inner loop for number of iterations specified in parameter file
            self.logger.info('Solving inner loop')
            tau_python.tau_solver_inner_loop(0, inner_n, min_residual)

            # Print monitoring values and increment physical time step
            tau_python.tau_solver_unsteady_monitoring()

            # Write results to files
            self.logger.info('Outputting results')

            # Write field data every fieldPeriod time steps if requested or if
            # it is the last iteration (to generate restart data)
            tau_python.tau_solver_write_output_conditional()

            # Finalize monitor history file (removes .tmp from file name)
            # tau_solver_output_monitor()

            if tau_python.tau_mpi_rank() == 0 and self.config.iteration % output_period == 0:
                self.tau2plt(str(self.config.parafile))

            tau_python.tau_parallel_sync()

            self.config.iteration += 1

            if deformation_defined:
                time = time_step * self.config.iteration
                scatfile_name = 'deform_{}.scat'.format(self.config.iteration)
                deformed_grid_name = './grid_deform/deform_{}.grid'.format(self.config.iteration)
                write_grid_deformation(scatfile_name, self.deformation_function(time))
                self.tau_para.update(self.set_para_primary_grid_name(primary_gridfile),
                                     para_replace=1)
                self.tau_para.update(self.set_para_deformed_mesh_name(deformed_grid_name),
                                     para_replace=1)
                self.tau_para.update(self.set_scatfile_name(scatfile_name),
                                     para_replace=1)
                self.tau_deform.run(read_primgrid=1, write_primgrid=1)
                self.tau_para.update(self.set_para_primary_grid_name(deformed_grid_name),
                                     para_replace=1)

            tau_python.tau_parallel_sync()

        tau_python.tau_solver_output_field()
        tau_python.tau_solver_output_surface()
        tau_python.tau_parallel_sync()
        self.tau_solver.stop()
        self.tau_solver.finalize()
