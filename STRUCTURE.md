# Code Structure
This file describes the structure of the TAU adapter.

# Basic Components
There are 4 basic classes in the TAU-preCICE adapter:
the Adapter class, the Solver class, the Interface class and the DataHandler 
class

# Adapter class
The Adapter class serves to control the Solver and Interface classes. The user controls
the simulation by modifying the config file and calling the Adapter class.

This class instantiates and controls the `Solver` and `Interface` classes.
The simulation can be run in coupled mode (with preCICE) or without preCICE,
depending on the configuration file.

# Solver class
The Solver class acts as a superclass for each Solver implementation. This
class is subclassed into the FlapSolver and HelicopterSolver classes, which
control the execution loop of the main simulation. 

# Interface class
This class controls the data movement to and from preCICE. During 
initialization,  the Interface class sets up various DataHandlers, which
handle data to/from preCICE. The Interface ensures that data is correctly
passed and received during information transfer

# DataHandler class
This class handles a single type of data from a single mesh in  preCICE. The
DataHandler either reads data from preCICE or writes data to preCICE.