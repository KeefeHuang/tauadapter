# Project Title

This is the TAU-preCICE adapter, written to allow the coupling of the TAU CFD 
solver to various other solid solvers. This solver is mainly designed to run 
both tightly-coupled (where coupled data is passed each timestep) and 
loosely-coupled (where coupled data is passed periodically) helicopter simulations 

## Getting Started

In order to get started, please ensure the following environmental variables are
set:
*  Ensure that the TAU-python library is in PYTHONPATH.\
 `export PYTHONPATH=$PYTHONPATH:\path\to\tau\library`
*  Ensure that the {ROOT_DIRECTORY} of this project is in PYTHONPATH\
 `export PYTHONPATH=$PYTHONPATH:\path\to\tauadapter`

These instructions will allow access to the library features on your 
local machine for running of the tutorials located at 
https://gitlab.lrz.de/KeefeHuang/tau-adapter-tutorials or for development and
testing purposes.

### Prerequisites

This project is mainly based in Python and has the following depencies:
* preCICE
* netCDF4
* Pyyaml
* numpy
* scipy
* jsonobject

## Running the tests

To run tests on the base classes, enter {ROOT_DIRECTORY}/tests and run 
`python -m unittest discover`.

The unit tests for each solver are also available. Navigate to
{ROOT_DIRECTORY}/{SOLVER_TYPE}/tests and run
`python -m unittest discover`


## Test Examples

Please refer to the https://gitlab.lrz.de/KeefeHuang/tau-adapter-tutorials gitlab repository
 for various example codes. There are currently three working tutorials (Flap, Deformation and 
 Helicopter). Below are short descriptions of each case:
 
 Flap: Tightly-coupled fluid-structure interaction case coupling TAU and CalculiX. Based off tutorial
 developed by Derek Risseeuw. Simulates the movement of an elastic perpendicular flap
 in channel flow.
 
 Helicopter: Loosely-coupled isolated rotorblade simulation coupling TAU and CAMRAD II. Based
  off coupled simulation developed by Aaron Carnefix. 
  
 Deformation: Loosely-coupled isolated rotorblade simulation coupling TAU and CAMRAD II. Based
  off coupled simulation developed by Aaron Carnefix. Extended to include elastic deformation during
  coupling.

## Authors

* **Huang Qunsheng** - *Initial work*


## Acknowledgments

* With thanks to Gerasimos Chourdakis and Amine Abdelmoula for their support and guidance in writing this code
