"""
@package tauadapter
Python wrapper to control the execution of TAU coupled with preCICE
"""
import logging
import logging.config
import subprocess
import sys
from abc import ABCMeta, abstractmethod
from os import makedirs
from os.path import isdir, join
import pickle
from mpi4py import MPI

import PyDeform  # pylint: disable=import-error
import PyPara  # pylint: disable=import-error
import PyPrep  # pylint: disable=import-error
import PySolv  # pylint: disable=import-error

from tauadapter.utility import config
from tauadapter.config import Config
from tauadapter.datahandlerfactory import DataHandlerFactory


class Solver(object):
    """ Abstract Solver base class that controls the simulation loop in TAU\n

    Attributes:
        config: Utility.Config dictionary object created from input config file,
        parsed with Utility.config()
        logger: Handle to logger that documents execution
        deformation_function: function used to calculate deformation values if TAU is run standalone
        tau_para: tau_python.PyPara object. Read and writes from/to TAU parafile
        tau_prep: tau_python.PyPrep object. Runs preprocessing in TAU
        tau_solver: tau_python.PySolver object. Controls the TAU solver.
        tau_deform: tau_python.PyDeform object. Controls grid deformation in TAU

    """
    __metaclass__ = ABCMeta
    CONFIG_CLASS = None

    # Class constructer, sets up tau-python classes
    def __init__(self, config_file, data_interface, config_class=Config):
        """ Constructor for solver base class.\n
        Sets up the various tau_python classes required to controls the TAU simulation

        Arguments:
            config_file (str): name of input config file as str
            data_interface (datainterface.DataInterface): DataInterface object, object that controls
            information passed between TAU and preCICE.
        """
        self.config_file = config_file
        if self.CONFIG_CLASS is not None and issubclass(self.CONFIG_CLASS, Config):
            self.config = config(config_file, self.CONFIG_CLASS)
        else:
            self.config = config(config_file, config_class)
        self.simulation = self.config.simulation
        self.data_interface = data_interface
        self.precice_interface = data_interface.precice_interface

        rank = MPI.COMM_WORLD.Get_rank()
        # Get configured logger
        self.logger = logging.getLogger('tau' + str(rank) + '.solver')

        # Note that in Python2.7 str-types from JSON Files are considered const* char
        # Run str() to convert to usable string format
        parafile = str(self.config.parafile)

        # Get instances of TAU-Python classes
        try:
            self.tau_para = PyPara.Parafile(parafile)
            self.tau_prep = PyPrep.Preprocessing(parafile)
            self.tau_solver = PySolv.Solver(parafile)
            self.tau_deform = PyDeform.Deformation(parafile)
        except IOError as error:
            self.logger.exception("Error: TAU-Python error. File '%s' not found.",
                                  error.filename)
            sys.exit()

        self.datahandlerfactory = DataHandlerFactory(self.config, self.precice_interface,
                                                     self.data_interface, self.tau_para)
        self.deformation_function = None

    def add_new_data_handler_type(self, class_dict, handler_type):
        """ Update dictionary of datahandler types

        Arguments:
            class_dict (dict): dict containing key-value pair, where key is the name of the new
                datahandler and value is the datahandler class type.
            handler_type (str): updates DATA_READERS or DATA_WRITES dict depending on input
        """
        self.datahandlerfactory.add_new_data_handler_type(class_dict, handler_type)

    # pylint: disable=too-many-locals, too-many-statements
    def setup_data_handlers(self):
        """ Implementation of abstract method in base class.
        Used to set up the various data handlers defined in config file
        """
        self.datahandlerfactory.initialize()

        restart_dict = self.get_restart_dict()

        # Loop through interface data from yaml config to set up data handlers.
        self.logger.info("Setting up data readers and writers.")
        for interface in self.config.interfaces:
            self.logger.debug("Setting up mesh %s", str(interface.mesh))
            self.datahandlerfactory.create_data_handlers(interface, restart_dict)

        self.logger.info("Completed setting up data readers and writers")
        self.save_restart_dict(restart_dict)

    def preprocessing(self):
        """ Runs preprocessing via the tau_python.PyPrep object.
        """
        self.tau_prep.run(write_dualgrid=1, free_primgrid=1, verbose=1)

    @abstractmethod
    def execute_precice(self):
        """ Abstract method that is unique to each solver case. Executes solver with preCICE.
        """
        pass

    @abstractmethod
    def execute(self):
        """ Abstract method that is unique to each solver case. Executes solver without preCICE.
        """
        pass

    # Function to call subprocess to run tau2plt and generate plt files
    @staticmethod
    def tau2plt(para_path):
        """ Calls subprocess to run tau2plt to generate plt file from latest TAU output file

        Arguments:
            para_path: path to TAU executables

        """
        lplt_proc = subprocess.Popen(['tau2plt', para_path])
        lplt_proc.communicate()
        return

    # Function to define the deformed mesh name in parafile
    @staticmethod
    def set_para_deformed_mesh_name(new_primary_grid):
        """ Returns a dict used to update the 'New primary grid prefix' in TAU parafile

        Arguments:
            new_primary_grid: new primary grid file as str

        Returns:
            para_list: dictionary containing new primary grid file
        """
        para_list = {'New primary grid prefix': new_primary_grid}
        return para_list

    @staticmethod
    def set_scatfile_name(scatfile_name):
        """ Returns a dict used to update the 'RBF basis coordinates and deflections filename' in
        TAU parafile

        Arguments:
            scatfile_name: new deformation scatfile as str

        Returns:
            para_list: dictionary containing new deformation gridfile
        """
        para_list = {'RBF basis coordinates and deflections filename': scatfile_name}
        return para_list

    @staticmethod
    def set_para_primary_grid_name(primary_grid_name):
        """ Returns a dict used to update the 'Primary grid prefix' in TAU parafile

        Arguments:
            primary_grid_name: new deformation scatfile as str

        Returns:
            para_list: dictionary containing primary gridfile prefix
        """

        para_list = {'Primary grid filename': primary_grid_name}
        return para_list

    @staticmethod
    def set_para_motion_file(motion_file_name):
        """ Returns a dict used to update the 'Primary grid prefix' in TAU parafile

        Arguments:
            motion_file_name: new deformation scatfile as str

        Returns:
            para_list: dictionary containing primary gridfile prefix
        """

        para_list = {'Motion description filename': motion_file_name}
        return para_list

    @staticmethod
    def set_para_motion_hierarchy_file(motion_file_name):
        """ Returns a dict used to update the 'Primary grid prefix' in TAU parafile

        Arguments:
            motion_file_name: new deformation scatfile as str

        Returns:
            para_list: dictionary containing primary gridfile prefix
        """

        para_list = {'Motion hierarchy filename': motion_file_name}
        return para_list

    @staticmethod
    def set_para_output_location(output_location):
        """ Returns a dict used to update the 'Primary grid prefix' in TAU parafile

        Arguments:
            output_location: new output location

        Returns:
            para_list: dictionary containing primary gridfile prefix
        """

        para_list = {'Output files prefix': output_location}
        return para_list

    @staticmethod
    def set_para_restart_filename(restart_filename):
        """ Returns a dict used to update the 'Primary grid prefix' in TAU parafile

        Arguments:
            restart_filename: name of restart file

        Returns:
            para_list: dictionary containing primary gridfile prefix
        """

        para_list = {'Restart-data prefix': restart_filename}
        return para_list

    def create_necessary_folder(self, name):
        """ Creates a necessary folder. If folder does not exist or cannot be created, the program
        will exist with error signal

        Arguments:
            name (str): key for folder in utility.Config object
        """
        folder = self.config[name]
        if not isdir(folder):
            try:
                makedirs(folder)
                self.logger.info("%s folder created", name)
            #     Broad exception is used because exceptions are stored in log file
            except Exception:  # pylint: disable=broad-except
                # broad exception allowed since we are storing exception data
                self.logger.exception("Unable to create a %s folder", name)
                sys.exit(1)

    def create_optional_folder(self, name):
        """ Creates an optional folder. If folder does not exist or cannot be created, the program
        will log this in the log file.

        Arguments:
            name (str): key for folder in utility.Config object
        """
        try:
            folder = self.config[name]
        except KeyError:
            self.logger.info("Variable %s not set in config file", name)
            folder = None

        if folder is not None:
            if not isdir(folder):
                try:
                    makedirs(folder)
                    self.logger.info("%s folder created", name)
                #     Broad exception is used because exceptions are stored in log file
                except Exception:  # pylint: disable=broad-except
                    # broad exception allowed since we are storing exception data
                    self.logger.exception(
                        "Unable to create a %s folder", name)

    def get_restart_dict(self):
        """ Function to get pickle dictionary of saved grid partitioning if so implemented. Also
        returns boolean indicating if partitioning of grid is required.

        Returns:
            None
        """
        rank = MPI.COMM_WORLD.Get_rank()
        restart_file = "restart" + str(rank) + ".pkl"
        restart_path = str(join(self.config.restart_location, restart_file))
        restart_dict = {}
        if self.config.partitioning:
            return restart_dict
        try:
            restart_dict = pickle.load(open(restart_path, "rb"))
            self.logger.debug("Restart dictionary %s found", restart_path)
        except (IOError, AttributeError):
            self.logger.warning("Restart dictionary %s not found, starting partitioning of primary "
                                "grid", restart_path)
            return restart_dict

        return restart_dict

    def save_restart_dict(self, restart_dict):
        """ Function to save pickle dictionary of saved grid partitioning
        Arguments:
            restart_dict (dict): Utility.Config dictionary object created from input config file,
            parsed with Utility.config()
        """
        rank = MPI.COMM_WORLD.Get_rank()
        restart_file = "restart" + str(rank) + ".pkl"
        restart_path = str(join(self.config.restart_location, restart_file))
        pickle.dump(restart_dict, open(restart_path, "wb"))
