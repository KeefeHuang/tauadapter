"""
@package tauadapter
This module contains the unittests for the read data handlers in interface.Interface class
"""
import unittest
from mock import Mock

import numpy as np

from utility import Config
from datainterface import DataInterface
from datahandler import DataHandler


# pylint: disable=attribute-defined-outside-init
class InterfaceReadDataHandlerTests(unittest.TestCase):
    """ InterfaceTests class for unittests for read data handlers interface.Interface class

    test_setup_read_data_handler: Tests the creation and handling of write data handlers
    test_read_data_handler_read: Tests the calling of the read function in read data handlers
    test_read_scalar_data: Tests the sending of scalar data
    test_read_vector_data: Tests the sending of vector data
    test_read_data_handler_write: Tests the calling of the write function in write data handlers
    """

    @classmethod
    def setUpClass(cls):
        """ unittest.TestCase set-up method. Used to setup environment for entire suite of
        unittests. Creates preCICE interface and utility.Config object for creation of
        interface.Interface object.
        """
        cls.test_config = Mock(spec=Config)
        cls.precice_interface = Mock()
        precice_attr = {'is_read_data_available.return_value': True,
                        'write_block_scalar_data.return_value': True,
                        'write_block_vector_data.return_value': True,
                        'read_block_scalar_data.return_value': True,
                        'read_block_vector_data.return_value': True,
                        'get_dimensions.return_value': 3}
        cls.precice_interface.configure_mock(**precice_attr)

    def setUp(self):
        """ Set-up method for each method in unittest.TestCase. Used to setup environment for entire
         suite of unittests. Creates DataInterface and adds data writers
        """
        mock_scalar_data_handler = Mock(spec=DataHandler)
        mock_vector_data_handler = Mock(spec=DataHandler)
        scalar_attr = {'write.side_effect': NotImplementedError,
                       'read.return_value': True,
                       'mesh_id': 0,
                       'data_id': [0],
                       'length': 3,
                       'precice_vertex_ids': [0, 1, 2],
                       'is_scalar.return_value': True}
        vector_attr = {'write.side_effect': NotImplementedError,
                       'read.return_value': True,
                       'mesh_id': 1,
                       'data_id': [1],
                       'length': 3,
                       'dimensions': 3,
                       'precice_vertex_ids': [0, 1, 2],
                       'is_scalar.return_value': False}
        mock_scalar_data_handler.configure_mock(**scalar_attr)
        mock_vector_data_handler.configure_mock(**vector_attr)
        self.interface = DataInterface(self.precice_interface, self.test_config)

        self.interface.add_read_data_hander(mock_scalar_data_handler)
        self.interface.add_read_data_hander(mock_vector_data_handler)

        self.interface.add_write_data_hander(mock_scalar_data_handler)
        self.interface.add_write_data_hander(mock_vector_data_handler)

    def tearDown(self):
        self.precice_interface.reset_mock()

    def test_setup_read_data_handler(self):
        """ Tests how interface.Interface class handles read data handlers.
        - Ensures that list of write data handlers/read data handlers is of correct length after
        creation of datahandlers.
        - Ensures created list contains the correct type of data
        """
        self.assertIsInstance(self.interface.data_readers, list)
        self.assertIsInstance(self.interface.data_writers, list)

        self.assertEqual(len(self.interface.data_readers), 2)
        self.assertEqual(len(self.interface.data_writers), 2)

        for read_data_handler in self.interface.data_readers:
            self.assertIsInstance(read_data_handler, DataHandler)

        for write_data_handler in self.interface.data_writers:
            self.assertIsInstance(write_data_handler, DataHandler)

    def test_read_data_handler_read(self):
        """ Tests how interface.Interface class handles read data handlers.
        - Ensures that list of read data handlers/write data handlers is of correct length after
        creation of datahandlers.
        """
        self.interface.read_all(0)
        for data_handler in self.interface.data_readers:
            self.assertTrue(data_handler.read.called)

    def test_read_scalar_data(self):
        """ Tests if correct precice write block data function is called if datahandler handler
        handles scalar data
        """
        self.interface.read(0, 0)
        data_id = 0
        length = 3
        precice_vertex_ids = [0, 1, 2]
        data = np.zeros(3)

        scalar_call_args = self.precice_interface.read_block_scalar_data.call_args[0]
        self.assertEqual(scalar_call_args[0], data_id)
        self.assertEqual(scalar_call_args[1], length)
        self.assertEqual(scalar_call_args[2], precice_vertex_ids)
        np.testing.assert_array_equal(data, scalar_call_args[3])

        vector_call_args = self.precice_interface.read_block_vector_data.call_args
        self.assertEqual(vector_call_args, None)

    def test_read_vector_data(self):
        """ Tests if correct precice write block data function is called if datahandler handler
        handles vector data
        """
        self.interface.read(0, 1)
        data_id = 1
        length = 3
        precice_vertex_ids = [0, 1, 2]
        data = np.zeros(9)

        scalar_call_args = self.precice_interface.read_block_scalar_data.call_args
        self.assertEqual(scalar_call_args, None)

        vector_call_args = self.precice_interface.read_block_vector_data.call_args[0]
        self.assertEqual(vector_call_args[0], data_id)
        self.assertEqual(vector_call_args[1], length)
        self.assertEqual(vector_call_args[2], precice_vertex_ids)
        np.testing.assert_array_equal(data, vector_call_args[3])

    def test_read_data_handler_write(self):
        """ Tests how interface.Interface class handles read data handlers. Checks if data handler
        will call the read function in datahandler. Checks that NotImplementedError is called if
        a data writer is treated as a data reader
        """

        with self.assertRaises(NotImplementedError):
            self.interface.write_all(0)

        scalar_call_args = self.precice_interface.write_block_scalar_data.call_args
        self.assertEqual(scalar_call_args, None)
        vector_call_args = self.precice_interface.write_block_vector_data.call_args
        self.assertEqual(vector_call_args, None)


if __name__ == "__main__":
    unittest.main()  # pylint: disable=no-member
