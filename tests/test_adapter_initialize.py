"""
@package tauadapter
This module contains the unittests for the initialization of the adapter class
"""
# pylint: disable=wrong-import-position
import unittest
import os
import glob
import sys
from mock import patch, Mock, MagicMock

sys.modules['tau_python'] = MagicMock()
sys.modules['precice'] = MagicMock()
sys.modules['PyPara'] = MagicMock()
sys.modules['PySolv'] = MagicMock()
sys.modules['PyPrep'] = MagicMock()
sys.modules['PyDeform'] = MagicMock()

from adapter import Adapter
from utility import Config


# pylint: disable=attribute-defined-outside-init, unused-argument, arguments-differ
class AdapterInitializeTests(unittest.TestCase):
    """ unittest.TestCase created to run unittests on adapter.Adapter class
    """

    @classmethod
    def setUpClass(cls):
        """ unittest.TestCase set-up method. Used to setup environment for entire suite of
        unittests. Creates preCICE interface and utility.Config object for creation of
        interface.Interface object.
        """
        cls.test_config = Mock(spec=Config)
        cls.test_config.logging_location = ""
        cls.test_config.logging_prefix = "log"

    @patch('utility.initialize_logging')
    @patch('utility.config')
    def setUp(self, mock_config, mock_logging):
        """ Setup method to create mock config file for each test case
        """
        self.adapter = Adapter("tests/test-config.yaml")

    # def test_initialize_existing_solver(self):
    #     """ Test if existing solver can be initialized with config file.
    #
    #     Arguments:
    #         mock_solver (Mock): Mock object that mocks the flap.flapsolver.FlapSolver class
    #     """
    #     self.adapter.config.solver = "Flap"
    #     self.adapter.initialize_solver()

    def test_initialize_nonexisting_solver(self):
        """ Test if correct error is thrown trying to instantiate non-existent class
        """
        self.adapter.config.solver = "Does Not Exist"
        with self.assertRaises(SystemExit):
            self.adapter.initialize_solver()

    # def test_initialize_tau_python(self):
    #     """ Test if taupython environment can be initialized
    #     """
    #     self.adapter.config.parafile = "test_parafile"
    #     self.adapter.initialize_tau_python()
    #     self.assertEqual(0, tau_python.tau_mpi_rank())

    # @patch('solver.Solver.execute_precice')
    # @patch('solver.Solver.execute')
    # def test_execute(self, mock_execute, mock_precice_execute):
    #     """ Test if correct execute function is called without precice
    #
    #     Arguments:
    #         mock_execute (Mock): Mock function to replace execute function
    #     """
    #     self.adapter.config.precice = False
    #     self.adapter.execute()
    #     self.assertTrue(mock_execute.called)
    #     self.assertFalse(mock_precice_execute.called)
    #
    # @patch('solver.Solver.execute_precice')
    # @patch('solver.Solver.execute')
    # def test_precice_execute(self, mock_execute, mock_precice_execute):
    #     """ Test if correct execute function is called with precice
    #
    #     Arguments:
    #         mock_execute (Mock): Mock function to replace execute function
    #     """
    #     self.adapter.config.precice = True
    #     self.adapter.execute()
    #     self.assertFalse(mock_execute.called)
    #     self.assertTrue(mock_precice_execute.called)

    @classmethod
    def tearDownClass(cls):
        file_list = glob.glob("./*.stderr") + glob.glob("./*.stdout")
        for file_path in file_list:
            try:
                os.remove(file_path)
            except OSError:
                pass


if __name__ == "__main__":
    unittest.main()
