"""
@package tauadapter
This module contains the unittests for the utility functions
"""
import unittest
import math
import filecmp
import os
import numpy as np
import utility


# pylint: disable=attribute-defined-outside-init
class UtilityUnitTests(unittest.TestCase):
    """ Utility class for unittests
    """

    def test_read_grid(self):
        """Test read_grid method. A simple TAU grid file is provided. Checks to see if read input
        has the right keys. Checks if point_yc array is correct with sample *.npy file.
        """
        grid_name = "tests/flap.grid"
        grid_data = utility.read_grid(grid_name)
        self.assertTrue("points_yc" in grid_data)
        self.assertTrue("points_xc" in grid_data)
        self.assertTrue("points_zc" in grid_data)
        self.assertTrue("points_of_surfacequadrilaterals" in grid_data)
        self.assertTrue("marker" in grid_data)
        self.assertTrue("points_of_hexaeders" in grid_data)

        points_yc = np.array(grid_data['points_yc'])
        stored_points_yc = np.load("tests/points_yc.npy")
        for i in range(points_yc.shape[0]):
            self.assertEqual(stored_points_yc[i], points_yc[i])

    # def test_write_deformation_grid(self):
    #     """ Test write_deformation_grid method. Writes a pre-defined deformation data and compares
    #     with existing 'deformation_sample.txt'. The two files should match exactly.
    #     """
    #     deformation_data = [0, 0, 0, 1, 2, 3, 0, 0, 1, 4, 5, 6, 0, 1, 0, 7, 8, 9,
    #                         0, 1, 1, 10, 11, 12, 1, 0, 0, 13, 14, 15]
    #     sample_filename = "tests/deformation_sample.txt"
    #     deformation_filename = "tests/deformation_test.txt"
    #     utility.write_grid_deformation(deformation_filename, deformation_data)
    #     self.assertTrue(filecmp.cmp(sample_filename, deformation_filename))
    #     os.remove(deformation_filename)
    def test_write_deformation_grid_not_divisible(self):
        """ Test write_deformation_grid method. Data passed to write_deformation_grid must be
        divisible by 6. Pass data that does not meet this criteria. Test should fail with
        AssertionError
        """
        deformation_data = [0, 0, 0, 1, 2, 3, 0, 0, 1, 4, 5, 6, 0, 1, 0, 7, 8, 9,
                            0, 1, 1, 10, 11, 12, 1, 0, 0, 13, 14]
        deformation_filename = "deformation_test.txt"

        with self.assertRaises(AssertionError):
            utility.write_grid_deformation(deformation_filename, deformation_data)

    def test_write_deformation_grid_not_1d(self):
        """ Test write_deformation_grid method. Data passed to write_deformation_grid must be
        a 1D array or list. Pass data that does not meet this criteria. Test should fail with
        AssertionError
        """
        deformation_data = [[1, 2, 3], [4, 5, 6]]
        deformation_filename = "deformation_test.txt"

        with self.assertRaises(AssertionError):
            utility.write_grid_deformation(deformation_filename, deformation_data)

    @staticmethod
    def test_swap_axis():
        """ Test swap_axis_yz method. Pass a predefined list to tested method. Assert that the
        output is correct.
        """
        test_array = [0, 2, 1, 3, 5, 4, 6, 8, 7]
        output_array = utility.swap_axis_yz(test_array)
        sample_array = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        np.testing.assert_array_equal(output_array, sample_array)

    def test_swap_axis_not_divisible(self):
        """ Test swap_axis_yz method. Pass a list of wrong length (not divisble by 3) to tested
        method. Test should raise an AssertError
        """
        test_array = [0, 2, 1, 3, 5, 4, 6, 8]
        with self.assertRaises(AssertionError):
            utility.swap_axis_yz(test_array)

    @staticmethod
    def test_rotation_matrix():
        """ Tests rotation_matrix method. Creates predefined point and a set rotation. The given
        sample rotations are easily calculable by hand.
        """
        point = [1, 0, 0]
        angle = 90
        rotation_matrix = utility.rotation_matrix(angle, 3, dim='deg')
        rot_point = rotation_matrix.dot(point)
        np.testing.assert_almost_equal(rot_point, [0.0, 1.0, 0.0])

        point = [0, 1, 0]
        angle = 60
        rotation_matrix = utility.rotation_matrix(angle, 1, dim='deg')
        rot_point = rotation_matrix.dot(point)
        np.testing.assert_almost_equal(rot_point, [0.0, 0.5, pow(3, 0.5) / 2])

        point = [0, 0, 1]
        angle = 45
        rotation_matrix = utility.rotation_matrix(angle, 2, dim='deg')
        rot_point = rotation_matrix.dot(point)
        np.testing.assert_almost_equal(rot_point, [pow(2, 0.5) / 2, 0.0, pow(2, 0.5) / 2])

    @staticmethod
    def test_sparse_rotation_matrix():
        """ Tests sparse rotation_matrix method. Creates predefined point and a set rotation. The
        given sample rotations are easily calculable by hand.
        """
        point = [1, 0, 0]
        angle = 90
        rotation_matrix = utility.sparse_rotation_matrix(angle, 3, dim='deg')
        rot_point = rotation_matrix.dot(point)
        np.testing.assert_almost_equal(rot_point, [0.0, 1.0, 0.0])

        point = [0, 1, 0]
        angle = 60
        rotation_matrix = utility.sparse_rotation_matrix(angle, 1, dim='deg')
        rot_point = rotation_matrix.dot(point)
        np.testing.assert_almost_equal(rot_point, [0.0, 0.5, pow(3, 0.5) / 2])

        point = [0, 0, 1]
        angle = 45
        rotation_matrix = utility.sparse_rotation_matrix(angle, 2, dim='deg')
        rot_point = rotation_matrix.dot(point)
        np.testing.assert_almost_equal(rot_point, [pow(2, 0.5) / 2, 0.0, pow(2, 0.5) / 2])

    def test_shift_coefficients(self):
        """ Tests shift coefficients. Gives sample coefficients and arbitrary shift. Check for
        correctness.
        """
        rotation_coefficients = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])

        sample_result = rotation_coefficients[0] \
                        + rotation_coefficients[1] * math.cos(0.12 + math.pi / 3) \
                        + rotation_coefficients[2] * math.sin(0.12 + math.pi / 3) \
                        + rotation_coefficients[3] * math.cos(0.24 + math.pi / 3) \
                        + rotation_coefficients[4] * math.sin(0.24 + math.pi / 3) \
                        + rotation_coefficients[5] * math.cos(0.36 + math.pi / 3) \
                        + rotation_coefficients[6] * math.sin(0.36 + math.pi / 3) \
                        + rotation_coefficients[7] * math.cos(0.48 + math.pi / 3) \
                        + rotation_coefficients[8] * math.sin(0.48 + math.pi / 3)

        shift = math.pi / 3
        shifted_coefficients = utility.shift_coefficients(rotation_coefficients, shift)

        result = shifted_coefficients[0] \
                 + shifted_coefficients[1] * math.cos(0.12) \
                 + shifted_coefficients[2] * math.sin(0.12) \
                 + shifted_coefficients[3] * math.cos(0.24) \
                 + shifted_coefficients[4] * math.sin(0.24) \
                 + shifted_coefficients[5] * math.cos(0.36) \
                 + shifted_coefficients[6] * math.sin(0.36) \
                 + shifted_coefficients[7] * math.cos(0.48) \
                 + shifted_coefficients[8] * math.sin(0.48)

        self.assertAlmostEqual(result, sample_result)

        shifted_coefficients = utility.shift_coefficients(shifted_coefficients, 0.0)

    @classmethod
    def tearDownClass(cls):
        """ Teardown method to remove unwanted created files if tests fail
        """
        deformation_filename = "deformation_test.txt"
        try:
            os.remove(deformation_filename)
        except OSError:
            pass


if __name__ == "__main__":
    unittest.main()
